
// === Created by mad_daddy ===

#if defined(_WIN32)
#include "../Core/MiniDump.h"
#include <windows.h>
#ifdef _MSC_VER
#include <crtdbg.h>
#endif
#endif

/// std
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

/// Cloth 
#include "transfer.h"
#include "api.h"

#include "Cloth.h"

/// igl
#include <igl/readOBJ.h>
#include <igl/colon.h>
#include <igl/harmonic.h>


/// cppcms
//#include <cppcms/application.h>
//#include <cppcms/service.h>
//#include <cppcms/applications_pool.h>
//#include <cppcms/rpc_json.h>
//#include "api.h"

// Back image path 
//std::string g_Back_Img_Path_;
//std::string g_M_49_Path_;
//
//std::string g_Cloth_Img_Path_;
//std::string g_Cloth_Contour_Path_;
//std::string g_Cloth_Mesh_Path_;
//
//// out texture path
//std::string g_Out_Path_;


// --- OS specific macro ---
/// Win32 execute
#if defined(_WIN32)
#define DEFINE_MAIN(function) \
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd) \
{ \
    return function; \
}

/// Linux or OS X: use main
#else
#define DEFINE_MAIN(function) \
int main(int argc, char** argv) \
{ \
    return function(argc, argv); \
}
#endif

int Transfer(const std::string b_i, // backimage
	const std::string m_49, // 49 points
	const std::string c_i, // cloth image
	const std::string c_c, // cloth contour
	const std::string c_m, // cloth mesh
	const std::string out) // output path
{
	g_Back_Img_Path_ = b_i;
	g_M_49_Path_ = m_49;
	g_Cloth_Img_Path_ = c_i;
	g_Cloth_Contour_Path_ = c_c;
	g_Cloth_Mesh_Path_ = c_m;
	g_Out_Path_ = out;

	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Urho3D::SharedPtr<ClothEngine> application(new ClothEngine(context));

	return application->Run();
}

// Main Clothing Demo entry point
int RunClothEngine()
{
	Urho3D::SharedPtr<Urho3D::Context> context(new Urho3D::Context());
	Urho3D::SharedPtr<ClothEngine> application(new ClothEngine(context));

	return application->Run();
}

// Main Clothing Demo entry point (Server)
int RunClothEngineSE(int argc, char** argv)
{
	/*try
	{
		cppcms::service srv(argc, argv);
		srv.applications_pool().mount(cppcms::applications_factory<api>());
		srv.run();
	}
	catch (std::exception const &e)
	{
		std::cerr << e.what() << std::endl;
		return 1;
	}*/

	return 0;
}


// Define Main func
#if defined(_WIN32)
DEFINE_MAIN(RunClothEngine());
#else
DEFINE_MAIN(RunClothEngineSE);
#endif


// ------ Base Stuff -------
void ClothEngine::CreateScene()
{
	URHO3D_LOGINFO("=== CreateScene() ===");
	scene_ = new Scene(context_);

	// Create octree, use default volume (-1000, -1000, -1000) to (1000, 1000, 1000)
	// Also create a DebugRenderer component so that we can draw debug geometry
	scene_->CreateComponent<Octree>();
	scene_->CreateComponent<DebugRenderer>();

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	// Create static scene content. First create a zone for ambient lighting and fog control
	Node* zoneNode = scene_->CreateChild("Zone");
	Zone* zone = zoneNode->CreateComponent<Zone>();
	/*zone->SetAmbientColor(Color(0.15f, 0.15f, 0.15f));
	zone->SetFogColor(Color(0.5f, 0.5f, 0.7f));
	zone->SetFogStart(100.0f);
	zone->SetFogEnd(300.0f);*/
	zone->SetBoundingBox(BoundingBox(-1000.0f, 1000.0f));

	// Create a directional light 
	{
		Node* lightNode = scene_->CreateChild("DirLight");
		lightNode->SetPosition(Vector3(25.0f, 30.0f, -25.0f));
		lightNode->LookAt(Vector3(0.0f, 8.0f, 0.0f));
		Light* light = lightNode->CreateComponent<Light>();
		light->SetLightType(LIGHT_DIRECTIONAL);
		light->SetCastShadows(false);
		light->SetSpecularIntensity(0.5f);
	}


	//// Init grid
	//if (drawGrid_)
	//{
	//	URHO3D_LOGINFO("=== Init grid | drawGrid_ - " + String(drawGrid_));

	//	Node* gridNode = scene_->CreateChild("Grid");
	//	CustomGeometry* grid = gridNode->CreateComponent<CustomGeometry>();
	//	grid->SetNumGeometries(1u);
	//	grid->SetMaterial(cache->GetResource<Material>("Materials/VColUnlit.xml"));
	//	grid->SetViewMask(0x80000000);
	//	grid->SetOccludee(false);

	//	UpdateGrid();
	//}
}

void ClothEngine::CreateUI()
{
	URHO3D_LOGINFO("=== CreateUI() ===");

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	UI* ui = GetSubsystem<UI>();

	// Set up global UI style into the root UI element
	XMLFile* style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
	ui->GetRoot()->SetDefaultStyle(style);

	// Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
	// control the camera, and when visible, it will interact with the UI
	SharedPtr<Cursor> cursor(new Cursor(context_));
	cursor->SetStyleAuto();
	ui->SetCursor(cursor);

	// Set starting position of the cursor at the rendering window center
	Graphics* graphics = GetSubsystem<Graphics>();

	int c_width = graphics->GetWidth();
	int c_height = graphics->GetHeight();

	URHO3D_LOGINFO("	c_width: " + String(c_width) + " | c_height: " + String(c_height) + "");

	cursor->SetPosition(c_width / 2, (c_height / 2) - 10);
}


// --- ViewRaycast ---
void ClothEngine::ViewRaycast(bool left_Click, bool middle_click)
{
	URHO3D_LOGINFO("--- ViewRaycast() |  left_Click - "+String(left_Click)+" middle_click - "+String(middle_click)+" ---");

	UI* ui = GetSubsystem<UI>();
	IntVector2 pos = ui->GetCursorPosition();

	// Check the cursor is visible and there is no UI element in front of the cursor
	if (!ui->GetCursor()->IsVisible() || ui->GetElementAt(pos, true))
		return;

	Graphics* graphics = GetSubsystem<Graphics>();
	Camera* camera = cameraNode_->GetComponent<Camera>();

	Ray cameraRay = camera->GetScreenRay(
		(float)pos.x_ / graphics->GetWidth(),
		(float)pos.y_ / graphics->GetHeight()
		);

	PODVector<RayQueryResult> results;
	RayOctreeQuery query(
		results,
		cameraRay,
		RAY_TRIANGLE,
		camera->GetFarClip(),
		DRAWABLE_GEOMETRY,
		0x7fffffff
		);

	scene_->GetComponent<Octree>()->RaycastSingle(query);

	// check result
	if (left_Click)
	{
		// Skip ClothPlane
		if (!results.Empty() && results[0].node_)
		{
			if (results[0].node_->GetName().Contains("ClothPlane"))
			{
				URHO3D_LOGERROR("=> SELECTED NODE - " + results[0].node_->GetName() + "");
				return;
			}
		}

		++LEFT_START_CLICK;
		//URHO3D_LOGINFO("LEFT_START_CLICK - " + String(LEFT_START_CLICK));

		// FIRST-1
		if (!results.Empty() && LEFT_START_CLICK == 1)
		{
			if (results[0].node_)
			{
				m_LastSlectedElement = results[0].node_;
				URHO3D_LOGINFO("=== m_LastSlectedElement: " + m_LastSlectedElement->GetName());
			
			}

			if (results[0].drawable_)
			{
				mpw_LastSelectedDrawable_ = results[0].drawable_;
				results[0].drawable_->DrawDebugGeometry(scene_->GetComponent<DebugRenderer>(), false);
			}

			LEFT_START_CLICK = 0;
			m_LastSlectedElement = nullptr;

			//URHO3D_LOGINFO(" |FIRST-1| LEFT_START_CLICK - " + String(LEFT_START_CLICK));
			return;

			
		}

		// FIRST-2
		if (!results.Empty() && LEFT_START_CLICK == 2)
		{
			LEFT_START_CLICK = 0;

			if (results[0].drawable_)
				results[0].drawable_->DrawDebugGeometry(scene_->GetComponent<DebugRenderer>(), false);

			//URHO3D_LOGINFO(" |FIRST-2| LEFT_START_CLICK - " + String(LEFT_START_CLICK));
		}

		// SECOND-1
		if (results.Empty() && LEFT_START_CLICK == 1)
		{
			m_LastSlectedElement = nullptr;
			mpw_LastSelectedDrawable_ = nullptr;
			LEFT_START_CLICK = 0;
		}

		// SECOND-2
		if (results.Empty() && LEFT_START_CLICK == 2)
		{
			if (m_LastSlectedElement)
			{
				// check type of selected node
				//int proj_type = CheckSelelectedNode();

				//URHO3D_LOGINFO("=== m_LastSlectedElement: " + m_LastSlectedElement->GetName() + " | proj_type: " + String(proj_type));
				URHO3D_LOGINFO("=== m_LastSlectedElement: " + m_LastSlectedElement->GetName());

				//	if (proj_type != -1)
				//	{
				//		Vector3 v_new = Vector3::ZERO;
				//		Vector3 v_last = Vector3::ZERO;

				//		Camera* camera = cameraNode_->GetComponent<Camera>();
				//		if (camera)
				//		{
				//			Renderer* p_render = GetSubsystem<Renderer>();
				//			if (p_render)
				//			{
				//				Viewport* p_view = p_render->GetViewport(0);
				//				if (p_view)
				//				{
				//					v_new = p_view->ScreenToWorldPoint(pos.x_, pos.y_, mf_Z_Dist_);
				//				}
				//			}

				//			URHO3D_LOGINFO("m_LastSlectedElement v_new: " + v_new.ToString());
				//		}

				//		if (proj_type == 0)
				//		{
				//			v_last = m_LastSlectedElement->GetWorldPosition();
				//			URHO3D_LOGINFO(" 0	m_LastSlectedElement v_last: " + v_last.ToString());
				//		}
				//	}
				//}

				LEFT_START_CLICK = 0;
			}
		}
	}

	if (middle_click)
	{
		PickClothTextuteContour(pos);
		//mb_drawClothContour_ = true;
	}

	//URHO3D_LOGINFO("### END ViewRaycast() ###");
}

// camera moving
void ClothEngine::MoveCamera(float timeStep)
{
	// Right mouse button controls mouse cursor visibility: hide when pressed
	UI* ui = GetSubsystem<UI>();
	Input* input = GetSubsystem<Input>();
	ui->GetCursor()->SetVisible(!input->GetMouseButtonDown(MOUSEB_RIGHT));

	// Do not move if the UI has a focused element
	if (ui->GetFocusElement())
		return;
}


/// ------ CLOTH ------
void ClothEngine::InitDefaultMENPOBackImage()
{
	Graphics* pGraphics = GetSubsystem<Graphics>();
	ResourceCache* cache = GetSubsystem<ResourceCache>();

	if (!mp_BackImage)
	{
		URHO3D_LOGINFO("=== InitDefaultMENPOBackImage ===");
		{
			SharedPtr<Texture2D> pTexture(new Texture2D(context_));

			pTexture->SetSize(pGraphics->GetWidth(), pGraphics->GetHeight(), Graphics::GetRGBFormat(), TEXTURE_RENDERTARGET);
			pTexture->SetFilterMode(Urho3D::FILTER_DEFAULT);
			pTexture->SetName("LayerTex");
			mp_BackImage = pTexture;

			if (cache->AddManualResource(mp_BackImage))
			{
				URHO3D_LOGINFO("	mp_BackImage initialized...");
			}
		}
	}
}

bool ClothEngine::LoadBackImage(const String& path, bool apply_scale)
{
	if (path.Empty())
		return false;

	if (!mp_BackImage)
		InitDefaultMENPOBackImage();

	URHO3D_LOGINFO("=== LoadBackImage | path: " + path + " ===");

	// global subsystems handlers
	Graphics* p_gr = GetSubsystem<Graphics>();
	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// get camera aspect ration - default
	Camera* camera = cameraNode_->GetComponent<Camera>();
	float aspect_ratio_def = camera->GetAspectRatio();
	bool isAutoAspect = camera->GetAutoAspectRatio();

	// load image
	SharedPtr<Image> p_Image(cache->GetResource<Image>(path));
	if (p_Image)
	{
		mu_BackImgInitWidth = p_Image->GetWidth();
		mi_BackImgWidth = mu_BackImgInitWidth;

		mu_BackImgInitHeight = p_Image->GetHeight();
		mi_BackImgHeight = mu_BackImgInitHeight;

		if (_LOGGING_)
		{
			URHO3D_LOGINFO("	mu_BackImgInitWidth: " + String(mu_BackImgInitWidth) +
				" | mu_BackImgInitHeight: " + String(mu_BackImgInitHeight));
		}
		
		if (mu_BackImgInitWidth > 0 && mu_BackImgInitHeight > 0)
		{
			// Calculate BackImage scale
			Pair<float, float> back_img_scale = CalcBackImgScale(mu_BackImgInitWidth, mu_BackImgInitHeight);

			if (!apply_scale)
			{
				back_img_scale.first_ = 1.0f;
				back_img_scale.second_ = 1.0f;
			}

			if (back_img_scale.first_ > 0.0f)
			{
				// save BackImage scale
				mf_BackImgHeightScale_ = back_img_scale.first_;

				int new_textureWidth = static_cast<int>(mu_BackImgInitWidth * back_img_scale.first_);
				int new_textureHeight = static_cast<int>(mu_BackImgInitHeight * back_img_scale.first_);

				if (_LOGGING_)
				{
					URHO3D_LOGINFO("back_img_scale X: " + String(back_img_scale.first_) +
					" | back_img_scale Y: " + String(back_img_scale.second_));
				}

				// resize loaded image
				if (p_Image->Resize(new_textureWidth, new_textureHeight))
				{
					if (_LOGGING_)
					{
						URHO3D_LOGINFO(" IMAGE new_textureWidth: " + String(new_textureWidth) +
						" | new_textureHeight: " + String(new_textureHeight));
					}

					// save
					mi_BackImgHeight = new_textureHeight;
					mi_BackImgWidth = new_textureWidth;

					// new aspect ratio
					float aspect_ratio_new = static_cast<float>(mi_BackImgWidth) / static_cast<float>(mi_BackImgHeight);

					// set main screen resolution from image size
					if (p_gr->SetMode(new_textureWidth, new_textureHeight))
					{
						camera->SetAspectRatio(aspect_ratio_new);
						isAutoAspect = camera->GetAutoAspectRatio();

						if (_LOGGING_)
						{
							URHO3D_LOGINFO("-> aspect_ratio_new - " + String(aspect_ratio_new) + " | REAL - "
								+ String(camera->GetAspectRatio()) + " isAutoAspect - " + String(isAutoAspect) + "");
						}

						// set main window position
						p_gr->SetWindowPosition(p_gr->GetWidth() / 2, 0);

						// Set data to mp_BackImage texture
						if (mp_BackImage->SetData(p_Image))
							return true;
					}
				}
			}
		}
	}

	return false;
}

Pair<float, float> ClothEngine::CalcBackImgScale(const int t_width, const int t_height)
{
	URHO3D_LOGINFO("=== CalcModelProjectionsUIPosSize | t_width: " + String(t_width) +
		" | t_height: " + String(t_height) + " ===");

	// return <0.0> if tex is null size
	if (t_width <= 0 || t_height <= 0)
		return Pair<float, float>(0.0f, 0.0f);

	// Try to get screen res from SDL
	SDL_DisplayMode mode;
	SDL_GetDesktopDisplayMode(0, &mode);
	int screen_width = mode.w;
	int screen_height = mode.h;

	if (screen_width <= 0 || screen_height <= 0)
		return Pair<float, float>(0.0f, 0.0f);

	if (_LOGGING_)
	{
		URHO3D_LOGINFO("	SDL_DisplayMode | screen_width: " + String(screen_width) +
			" | screen_height: " + String(screen_height));
	}

	// Calc Back Image scale on given screen res
	float back_img_scale_h = 0.0f;
	float back_img_scale_w = 0.0f;
	Pair<float, float> res;

	float sf_height = static_cast<float>(screen_height);
	float tf_height = static_cast<float>(t_height);

	if(_LOGGING_)
	{
		URHO3D_LOGINFO("sf_height: " + String(sf_height) +
			" | tf_height: " + String(tf_height));
	}

	back_img_scale_h = sf_height / tf_height;

	float sf_width = static_cast<float>(screen_width);
	float tf_width = static_cast<float>(t_height);

	if(_LOGGING_)
	{
		URHO3D_LOGINFO("sf_width: " + String(sf_width) +
			" | tf_width: " + String(tf_width));
	}

	back_img_scale_w = sf_width / tf_width;

	mf_BackImage_scale_h = back_img_scale_h;
	mf_BackImage_scale_w = back_img_scale_w;

	// debug calculated new_width & new_height scale factor
	if (_LOGGING_)
	{
		URHO3D_LOGINFO("	mf_BackImage_scale_h: " + String(mf_BackImage_scale_h) +
			" | mf_BackImage_scale_w: " + String(mf_BackImage_scale_w));
	}

	res.first_ = mf_BackImage_scale_h;
	res.second_ = mf_BackImage_scale_w;

	return res;
}

// ------ M_49 ------
unsigned ClothEngine::LoadMENPOPointSetJSON(const String& path, bool remove_duplicates, SharedPtr<MenpoPS>& target)
{
	if (!target || target->m_bMPSProjAdjusted_)
		return 0;

	URHO3D_LOGINFO("=== LoadMENPOPointSetJSON | remove_duplicates - " + String(remove_duplicates) + " ===");

	target->m_ORIG_MenpoPS_List.Clear();
	target->m_PROJ_MenpoPS_List.Clear();
	target->MENPO_POINT_SET.Clear();

	FileSystem* filesystem = GetSubsystem<FileSystem>();

	if (filesystem->FileExists(path))
	{
		SharedPtr<File> SourceFile(new File(context_, path));
		SharedPtr<JSONFile> jsonfile(new JSONFile(context_));

		if (jsonfile->Load(*SourceFile))
		{
			URHO3D_LOGINFO("-> JSON file - " + path + " exist!");
			const JSONValue& rootVal = jsonfile->GetRoot();
			if (!rootVal.IsNull())
			{
				const JSONValue& rootVal = jsonfile->GetRoot();
				JSONValue landmarksVal = rootVal.Get("landmarks");

				// get points array
				bool is_array = landmarksVal.Get("points").IsArray();
				URHO3D_LOGINFO(" landmarks - " + String(is_array));
				JSONArray pointsArray = landmarksVal.Get("points").GetArray();

				unsigned points_cnt = pointsArray.Size();
				unsigned PointsArray_size = 0;

				URHO3D_LOGINFO(" points_cnt: " + String(points_cnt));
				for (unsigned i = 0; i < points_cnt; i++)
				{
					JSONValue PointVal = pointsArray.At(i);
					JSONArray Point = PointVal.GetArray();
					if (Point.Size() == 2)
					{
						Pair<unsigned, Vector2> point;
						point.first_ = i;
						point.second_ = Vector2(Point.At(1).GetFloat(), Point.At(0).GetFloat());

						target->m_ORIG_MenpoPS_List.Push(point);
					}
				}

				if (!target->m_ORIG_MenpoPS_List.Empty())
				{
					/*if(mp_MPS_OLD_)
					{
						for (unsigned j = 0; j < target->m_ORIG_MenpoPS_List.Size(); ++j)
						{
							URHO3D_LOGINFO("	P[" + String(target->m_ORIG_MenpoPS_List[j].first_) +
								"]: " + target->m_ORIG_MenpoPS_List[j].second_.ToString());
						}
					}*/

					// compute MENPO PS geom center
					Vector2 center = Vector2::ZERO;
					{
						for (auto i : target->m_ORIG_MenpoPS_List)
							center += i.second_;

						target->m_OrigMPointSetCtr_ = center / target->m_ORIG_MenpoPS_List.Size();
						URHO3D_LOGINFO("-> CENTER m_OrigMPointSetCtr_: " + target->m_OrigMPointSetCtr_.ToString());
					}

					URHO3D_LOGINFO("-> APPLY image_scale to m_ORIG_MenpoPS_List - " + String(mf_BackImgHeightScale_));
					{
						{
							for (auto& i : target->m_ORIG_MenpoPS_List)
								i.second_ *= mf_BackImgHeightScale_;

							target->m_OrigMPointSetCtr_ *= mf_BackImgHeightScale_;
						}
					}

					GetOrigMPScreenMeasures(target);

					return true;
				}
			}
		}
		else
		{
			URHO3D_LOGINFO("-> JSON file - " + path + " is NULL !!!");
			return false;
		}
	}
	else
		return false;
}

bool ClothEngine::GetOrigMPScreenMeasures(SharedPtr<MenpoPS>& target)
{
	URHO3D_LOGINFO("=== GetOrigMPScreenMeasures() ===");

	if (!target || target->m_ORIG_MenpoPS_List.Size() < 49)
		return false;

	// get original MPS screen height
	{
		Pair<unsigned, Vector2> m_8 = target->m_ORIG_MenpoPS_List.At(8);
		Pair<unsigned, Vector2> m_37 = target->m_ORIG_MenpoPS_List.At(37);

		target->m_OMPS_ScreenHeight_ = fabsf(m_8.second_.y_ - m_37.second_.y_);
		//URHO3D_LOGINFO("-> m_OMPS_ScreenHeight_: " + String(target->m_OMPS_ScreenHeight_));
	}

	// get original MPS Legs screen height
	{
		Pair<unsigned, Vector2> m_37 = target->m_ORIG_MenpoPS_List.At(37);
		Pair<unsigned, Vector2> m_29 = target->m_ORIG_MenpoPS_List.At(29);

		Vector3 res_screen_low_pos = VectorLerp(Vector3(m_37.second_, 0.0f), Vector3(m_29.second_, 0.0f), Vector3(0.5f, 0.5f, 0.5f));
		target->m_OMPS_Legs_ScreenHeight_ = fabsf(target->m_OrigMPointSetCtr_.y_ - res_screen_low_pos.y_);
		//URHO3D_LOGINFO("-> m_OMPS_Legs_ScreenHeight_: " + String(target->m_OMPS_Legs_ScreenHeight_));
	}

	// get original MPS Body screen height
	{
		Pair<unsigned, Vector2> m_5 = target->m_ORIG_MenpoPS_List.At(12);
		Pair<unsigned, Vector2> m_12 = target->m_ORIG_MenpoPS_List.At(5);

		Vector3 res_screen_neck_pos = VectorLerp(Vector3(m_5.second_, 0.0f), Vector3(m_12.second_, 0.0f), Vector3(0.5f, 0.5f, 0.5f));
		target->m_OMPS_Body_ScreenHeight_ = fabsf(res_screen_neck_pos.y_ - target->m_OrigMPointSetCtr_.y_);
		//URHO3D_LOGINFO("-> m_OMPS_Body_ScreenHeight_: " + String(target->m_OMPS_Body_ScreenHeight_));
	}

	return true;
}


bool ClothEngine::ProjectMORIGMenpoPS(const Vector2 p_scale, float depth, SharedPtr<MenpoPS>& target, bool create_nodes, bool save_f)
{
	if (!target || target->m_ORIG_MenpoPS_List.Empty())
		return false;

	if (target->m_bMPSProjAdjusted_)
		return false;

	URHO3D_LOGINFO("=== ProjectMOrigMenpoPS  | set_scale: " + p_scale.ToString() + " | depth: "+String(depth)+" ===");

	target->m_bMPSProjAdjusted_ = false;

	target->m_PROJ_MenpoPS_List.Clear();

	// remove scene nodes
	for (auto& i : target->MENPO_POINT_SET)
		i.second_->Remove();

	target->MENPO_POINT_SET.Clear();

	// global cache
	ResourceCache* cache = GetSubsystem<ResourceCache>();
	UI* ui = GetSubsystem<UI>();

	// Project OMPS from screeen coords
	{
		Camera* camera = cameraNode_->GetComponent<Camera>();
		if (camera)
		{
			Renderer* p_render = GetSubsystem<Renderer>();
			if (p_render)
			{
				Viewport* p_view = p_render->GetViewport(0);
				if (p_view)
				{
					URHO3D_LOGINFO("PROJECTED MPS depth: " + String(depth));
					for (auto& i : target->m_ORIG_MenpoPS_List)
					{
						//URHO3D_LOGINFO(" before	V["+String(i.first_)+"]: " + i.second_.ToString());

						int screen_x = static_cast<int>(i.second_.x_);
						int screen_y = static_cast<int>(i.second_.y_);

						Vector3 m_pos = p_view->ScreenToWorldPoint(screen_x, screen_y, depth);
						//URHO3D_LOGINFO("	V["+String(i.first_)+"]: " + m_pos.ToString());

						Pair<unsigned, Vector2> proj_pair;
						proj_pair.first_ = i.first_;
						proj_pair.second_ = Vector2(m_pos.x_, m_pos.y_);
						target->m_PROJ_MenpoPS_List.Push(proj_pair);
					}

					// Project OMPS geom Centr
					{
						int screen_x = static_cast<int>(target->m_OrigMPointSetCtr_.x_);
						int screen_y = static_cast<int>(target->m_OrigMPointSetCtr_.y_);

						Vector3 m_pos = p_view->ScreenToWorldPoint(screen_x, screen_y, depth);
						URHO3D_LOGINFO("-> m_ProjMPointSetCtr_: " + m_pos.ToString());
						target->m_ProjMPointSetCtr_.first_ = Vector2(m_pos.x_, m_pos.y_);
					}
				}
			}
		}
	}

	// === Save Menpo data
	/*if (save_f)
		SaveMenpoPROJ_PS("m_49", target);
*/
	// === Process Menpo center ===
	if (create_nodes)
	{
		// custom material with dissabled depth test
		SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_red.xml")->Clone();
		if (mat)
		{
			mat->SetRenderOrder(200);   // higher render order

			SharedPtr<Technique> tec = mat->GetTechnique(0)->Clone();
			if (tec)
				tec->GetPass(0)->SetDepthTestMode(CMP_ALWAYS);   // Always pass depth test

			mat->SetTechnique(0, tec);
		}

		target->m_ProjMPointSetCtr_.second_ = scene_->CreateChild("MPS_CENTER");
		StaticModel* box = target->m_ProjMPointSetCtr_.second_->CreateComponent<StaticModel>();
		box->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
		box->SetMaterial(mat);

		// set default pos & scale for visual scene object			
		target->m_ProjMPointSetCtr_.second_->SetPosition(Vector3(target->m_ProjMPointSetCtr_.first_.x_, target->m_ProjMPointSetCtr_.first_.y_, 0.0f));
		target->m_ProjMPointSetCtr_.second_->SetScale(Vector3(0.25f, 0.25f, 0.0f));

		// CLOTHING only
		target->m_ProjMPointSetCtr_.second_->SetEnabledRecursive(false);
	}

	// === Process Proj Menpo PointSet ===
	if (!target->m_PROJ_MenpoPS_List.Empty() && create_nodes)
	{
		// custom material with dissabled depth test
		SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_blue.xml")->Clone();
		if (mat)
		{
			mat->SetRenderOrder(200);   // higher render order

			SharedPtr<Technique> tec = mat->GetTechnique(0)->Clone();
			if (tec)
				tec->GetPass(0)->SetDepthTestMode(CMP_ALWAYS);   // Always pass depth test

			mat->SetTechnique(0, tec);
		}

		// vis Proj Menpo PointSet
		target->MENPO_POINT_SET.Reserve(target->m_PROJ_MenpoPS_List.Size());
		{
			for (auto& p : target->m_PROJ_MenpoPS_List)
			{
				Pair<unsigned, Node*> p_pair;
				p_pair.first_ = p.first_;
				p_pair.second_ = nullptr;

				target->MENPO_POINT_SET.Push(p_pair);
			}

            // blue points
			for (unsigned p = 0; p < target->m_PROJ_MenpoPS_List.Size(); ++p)
			{
				Pair<unsigned, Vector2>& P = target->m_PROJ_MenpoPS_List.At(p);

				target->MENPO_POINT_SET[p].first_ = P.first_;
				target->MENPO_POINT_SET[p].second_ = scene_->CreateChild("M_P" + String(P.first_));
				StaticModel* box = target->MENPO_POINT_SET[p].second_->CreateComponent<StaticModel>();
				box->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
				box->SetMaterial(mat);

				// set default pos & scale for visual scene object			
				target->MENPO_POINT_SET[p].second_->SetPosition(Vector3(P.second_.x_, P.second_.y_, 0.0f));
				target->MENPO_POINT_SET[p].second_->SetScale(Vector3(p_scale.x_, p_scale.y_, 0.0f));
				//target->MENPO_POINT_SET[p].second_->SetEnabledRecursive(draw);
				//target->MENPO_POINT_SET[p].second_->SetEnabled(draw);

				/*Node* TitleNode = target->MENPO_POINT_SET[p].second_->CreateChild();
				TitleNode->SetName("P_T" + String(P.first_));
				TitleNode->SetPosition(Vector3(0.0f, 1.2f, 0.0f));
				TitleNode->SetScale(Vector3(5.0f, 5.0f, 0.0f));
				Text3D* TitleText = TitleNode->CreateComponent<Text3D>();
				TitleText->SetText("M - " + String(P.first_));
				TitleText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.sdf"), 24);
				TitleText->SetFontSize(50);

				TitleText->SetColor(Color::WHITE);
				TitleText->SetTextEffect(TE_SHADOW);
				TitleText->SetEffectColor(Color(0.5f, 0.5f, 0.5f));
				TitleText->SetAlignment(HA_CENTER, VA_CENTER);*/
				//TitleText->SetEnabled(draw);		
				//target.MENPO_POINT_SET[p].second_->SetEnabledRecursive(draw);
			}
		}

		// projected flag
		if (target->MENPO_POINT_SET.Size() == target->m_PROJ_MenpoPS_List.Size())
			target->m_bMPSProjAdjusted_ = true;
	}

	// debug 
	{
		/*URHO3D_LOGINFO("m_PROJ_MenpoPS_List:");
		for (int i = 0; i < target->m_PROJ_MenpoPS_List.Size(); ++i)
		{
		URHO3D_LOGINFO("	V["+String(target->m_PROJ_MenpoPS_List[i].first_)+"]: "
		+ target->m_PROJ_MenpoPS_List[i].second_.ToString());
		}	*/
	}

	if (GetProjMPWorldMeasures(target) == 1)
		return true;

	/*if(!target->m_PROJ_MenpoPS_List.Empty())
	target->m_bMPSProjAdjusted_ = true;*/

	//if (target->m_bMPSProjAdjusted_)
	//{

	//URHO3D_LOGINFO("-> MENPO_POINT_SET: " + String(target->MENPO_POINT_SET.Size()));
	//URHO3D_LOGINFO("=== END ProjectMOrigMenpoPS ===");
	//return true;
	//}

	return false;
}

int ClothEngine::GetProjMPWorldMeasures(SharedPtr<MenpoPS>& target)
{
	if (!target || target->m_PROJ_MenpoPS_List.Size() < 49)
		return -1;

	// get PMPS world height
	{
		Pair<unsigned, Vector2> m_8 = target->m_PROJ_MenpoPS_List.At(8);
		Pair<unsigned, Vector2> m_37 = target->m_PROJ_MenpoPS_List.At(37);

		target->m_PMPS_WorldHeight_ = fabsf(m_8.second_.y_ - m_37.second_.y_);
		//URHO3D_LOGINFO("-> m_PMPS_WorldHeight_: " + String(target->m_PMPS_WorldHeight_));
	}

	// get PMPS Legs world height
	{
		Pair<unsigned, Vector2> m_37 = target->m_PROJ_MenpoPS_List.At(37);
		Pair<unsigned, Vector2> m_29 = target->m_PROJ_MenpoPS_List.At(29);

		target->mv3_PMPS_Legs_WP_ = VectorLerp(Vector3(m_37.second_, 0.0f), Vector3(m_29.second_, 0.0f), Vector3(0.5f, 0.5f, 0.5f));
		target->m_PMPS_Legs_WorldHeight_ = fabsf(target->m_ProjMPointSetCtr_.first_.y_ - target->mv3_PMPS_Legs_WP_.y_);
		//URHO3D_LOGINFO("-> m_PMPS_Legs_WorldHeight_: " + String(target->m_PMPS_Legs_WorldHeight_));
	}

	// get original MPS Body world height
	{
		Pair<unsigned, Vector2> m_5 = target->m_PROJ_MenpoPS_List.At(12);
		Pair<unsigned, Vector2> m_12 = target->m_PROJ_MenpoPS_List.At(5);

		target->mv3_PMPS_Body_WP_ = VectorLerp(Vector3(m_5.second_, 0.0f), Vector3(m_12.second_, 0.0f), Vector3(0.5f, 0.5f, 0.5f));
		target->m_OMPS_Body_WorldHeight_ = fabsf(target->mv3_PMPS_Body_WP_.y_ - target->m_ProjMPointSetCtr_.first_.y_);
		//URHO3D_LOGINFO("-> m_OMPS_Body_WorldHeight_: " + String(target->m_OMPS_Body_WorldHeight_));
	}

	return 1;
}

void ClothEngine::DrawMenpoPROJ_PS(SharedPtr<MenpoPS>& target) const
{

}


// ------ Cloth Contour ------
bool ClothEngine::LoadClothTexture(const String& path)
{
	if (path.Empty())
		return false;

	URHO3D_LOGINFO("=== LoadClothTexture() | path - " + path + " ===");

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// Clear previous Tex * scene nodes
	{
		// flags
		mb_drawClothContour_ = false;
		mb_ShowClothPlane_ = false;

		mf_ClothTex_scale_ = 0.0f;
		mf_ClothTex_scale_h = 0.0f;
		mf_ClothTex_scale_w = 0.0f;

		mi_ClothTex_height = 0;
		mi_ClothTex_width = 0;

		// remove scene nodes - Cloth contoour
		for (auto& i : mv_ClothTextureContour_)
			i.second_->Remove();


		if (m_Cloth_Plane_)
		{
			URHO3D_LOGINFO("=> CLEAR m_Cloth_Plane_...");
			m_Cloth_Plane_->Remove();
			//m_Cloth_Plane_->UnsubscribeFromAllEvents();
			//m_Cloth_Plane_->SetEnabledRecursive(false);
			m_Cloth_Plane_ = nullptr;
		}

		if (mp_ClothCenterNode_)
		{
			URHO3D_LOGINFO("=> CLEAR mp_ClothCenterNode_...");
			mp_ClothCenterNode_->Remove();
			//mp_ClothCenterNode_->UnsubscribeFromAllEvents();
			//mp_ClothCenterNode_->SetEnabledRecursive(false);
			mp_ClothCenterNode_ = nullptr;
		}

		if (mp_ClothTex_)
		{
			URHO3D_LOGINFO("=> CLEAR mp_ClothTex_...");
			//mp_ClothTex_->UnsubscribeFromAllEvents();
			//mp_ClothTex_.Reset();
			mp_ClothTex_ = nullptr;
		}

		// release additional Cloth data
		mv_ClothTextureContour_.Clear();
		mv_ClothTextureContourClone_.Clear();
		//mv_TriangulatedCloth_.Clear();

		// release UI 
		//ReleaseMT_UIButtons();

		URHO3D_LOGINFO("=> END CLEAR mp_ClothTex_ & other data...");
	}

	// load image
	SharedPtr<Image> p_ClothImage(cache->GetResource<Image>(path));
	if (p_ClothImage)
	{
		int textureWidth = p_ClothImage->GetWidth();

		mi_ClothTexW_ = textureWidth;
		mi_ClothTex_width = textureWidth;

		int textureHeight = p_ClothImage->GetHeight();
		mi_ClothTexH_ = textureHeight;
		mi_ClothTex_height = textureHeight;

		URHO3D_LOGINFO(" CLOTH TEX Width: " + String(textureWidth) + " | Height: " + String(textureHeight));

		if (textureWidth > 0 && textureHeight > 0)
		{
			SharedPtr<Texture2D> pTexture(new Texture2D(context_));
			//unsigned components_count = pTexture->GetComponents();
			//URHO3D_LOGINFO("=> components_count: " + String(components_count));

			//pTexture->SetSize(textureWidth, textureHeight, Graphics::GetRGBFormat(), TEXTURE_RENDERTARGET);
			//pTexture->SetSize(textureWidth, textureHeight, Graphics::GetRGBFormat(), TEXTURE_DYNAMIC);
			pTexture->SetSize(textureWidth, textureHeight, Graphics::GetRGBAFormat(), TEXTURE_DYNAMIC);

			//pTexture->SetFilterMode(Urho3D::FILTER_TRILINEAR);
			pTexture->SetFilterMode(Urho3D::FILTER_DEFAULT);
			pTexture->SetName("LayerClothTex");
			mp_ClothTex_ = pTexture;

			if (cache->AddManualResource(mp_ClothTex_))
			{
				URHO3D_LOGINFO("-> mp_ClothTex_ initialized...");
			}
			else
			{
				URHO3D_LOGERROR("!!! mp_ClothTex_ IS NULL !!!");
				return false;
			}

			// Set data to mp_ClothTex_ texture
			if (!mp_ClothTex_->SetData(p_ClothImage))
			{
				URHO3D_LOGERROR("!!! FAILED TO SET DATA TO <mp_ClothTex_> !!!");
				return false;
			}

			// compute cloth image center
			IntVector2 ClothImageCenter = IntVector2(textureWidth / 2, textureHeight / 2);
			URHO3D_LOGINFO("-> ClothImageCenter screen - " + ClothImageCenter.ToString());

			// get  MENPO PS-49 center in screen coords
			IntVector2 menpo_center;
			if (mp_MPS_OLD_)
			{
				menpo_center = IntVector2(static_cast<int>(mp_MPS_OLD_->m_OrigMPointSetCtr_.x_), static_cast<int>(mp_MPS_OLD_->m_OrigMPointSetCtr_.y_));
				URHO3D_LOGINFO("-> MENPO m_OrigMPointSetCtr_ screen - " + menpo_center.ToString());
			}
			else
			{
				URHO3D_LOGERROR("!!! MENPO IS NULL !!!");
				return false;
			}

			// project cloth image screen quad in world coords
			const int cloth_half_w = textureWidth / 2;
			const int cloth_half_h = textureHeight / 2;

			IntVector2 Cloth_scr_P0, Cloth_scr_P1, Cloth_scr_P2, Cloth_scr_P3;
			// P0
			Cloth_scr_P0.x_ = menpo_center.x_ - cloth_half_w;
			Cloth_scr_P0.y_ = menpo_center.y_ + cloth_half_h;

			// P1
			Cloth_scr_P1.x_ = menpo_center.x_ - cloth_half_w;
			Cloth_scr_P1.y_ = menpo_center.y_ - cloth_half_h;

			// P2
			Cloth_scr_P2.x_ = menpo_center.x_ + cloth_half_w;
			Cloth_scr_P2.y_ = menpo_center.y_ - cloth_half_h;

			// P3
			Cloth_scr_P3.x_ = menpo_center.x_ + cloth_half_w;
			Cloth_scr_P3.y_ = menpo_center.y_ + cloth_half_h;

			// world plane coords
			Vector3 Cloth_world_P0 = Vector3::ZERO;
			Vector3 Cloth_world_P1 = Vector3::ZERO;
			Vector3 Cloth_world_P2 = Vector3::ZERO;
			Vector3 Cloth_world_P3 = Vector3::ZERO;

			// world plane center
			Vector3 Cloth_world_Center = Vector3::ZERO;

			// Get world coords for visual plane
			{
				Camera* camera = cameraNode_->GetComponent<Camera>();
				if (camera)
				{
					Renderer* p_render = GetSubsystem<Renderer>();
					if (p_render)
					{
						Viewport* p_view = p_render->GetViewport(0);
						if (p_view)
						{
							Cloth_world_P0 = p_view->ScreenToWorldPoint(Cloth_scr_P0.x_, Cloth_scr_P0.y_, mf_Z_Dist_);
							Cloth_world_P0.z_ = 0.01f;
							Cloth_world_P1 = p_view->ScreenToWorldPoint(Cloth_scr_P1.x_, Cloth_scr_P1.y_, mf_Z_Dist_);
							Cloth_world_P1.z_ = 0.01f;
							Cloth_world_P2 = p_view->ScreenToWorldPoint(Cloth_scr_P2.x_, Cloth_scr_P2.y_, mf_Z_Dist_);
							Cloth_world_P2.z_ = 0.01f;
							Cloth_world_P3 = p_view->ScreenToWorldPoint(Cloth_scr_P3.x_, Cloth_scr_P3.y_, mf_Z_Dist_);
							Cloth_world_P3.z_ = 0.01f;

							/*mv_ClothTextureOffset_.Push(Pair<Vector3, Node*>(Cloth_world_P0, nullptr));
							mv_ClothTextureOffset_.Push(Pair<Vector3, Node*>(Cloth_world_P3, nullptr));
							mv_ClothTextureOffset_.Push(Pair<Vector3, Node*>(Cloth_world_P2, nullptr));
							mv_ClothTextureOffset_.Push(Pair<Vector3, Node*>(Cloth_world_P1, nullptr));*/

							//Cloth_world_Center = p_view->ScreenToWorldPoint(ClothImageCenter.x_, ClothImageCenter.y_, -30.0f);

							Vector3 Cloth_half_width = VectorLerp(Cloth_world_P0, Cloth_world_P3, Vector3(0.5f, 0.5f, 0.5f));
							Vector3 Cloth_half_height = VectorLerp(Cloth_world_P0, Cloth_world_P1, Vector3(0.5f, 0.5f, 0.5f));
							Cloth_world_Center = Vector3(Cloth_half_width.x_, Cloth_half_height.y_, Cloth_half_height.z_);
						}
					}
				}
			}

			// debug
			/*URHO3D_LOGINFO("--- CLOTH POSITION:");
			{
			URHO3D_LOGINFO("	P[0] screen: " + Cloth_scr_P0.ToString() + " | world: " + Cloth_world_P0.ToString());
			URHO3D_LOGINFO("	P[1] screen: " + Cloth_scr_P1.ToString() + " | world: " + Cloth_world_P1.ToString());
			URHO3D_LOGINFO("	P[2] screen: " + Cloth_scr_P2.ToString() + " | world: " + Cloth_world_P2.ToString());
			URHO3D_LOGINFO("	P[3] screen: " + Cloth_scr_P3.ToString() + " | world: " + Cloth_world_P3.ToString());

			URHO3D_LOGINFO("	Cloth_world_Center screen: " + ClothImageCenter.ToString() + " | world: " + Cloth_world_Center.ToString());
			}*/

			// compute plane scale
			float plane_width = SegLenght(Cloth_world_P0, Cloth_world_P3);
			float plane_height = SegLenght(Cloth_world_P0, Cloth_world_P1);
			URHO3D_LOGINFO("-> CLOTH PLANE <world> width: " + String(plane_width) + " | height: " + String(plane_height) + "");

			// construct a plane
			if (!m_Cloth_Plane_)
			{
				URHO3D_LOGINFO("=> CREATE new m_Cloth_Plane_...");

				// custom material with dissabled depth test
				//SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_red.xml")->Clone();
				SharedPtr<Material> mat = cache->GetResource<Material>("Materials/Mushroom.xml")->Clone();
				mat->SetTexture(TU_DIFFUSE, mp_ClothTex_);

				m_Cloth_Plane_ = scene_->CreateChild("ClothPlane");
				StaticModel* plane = m_Cloth_Plane_->CreateComponent<StaticModel>();
				plane->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
				plane->SetMaterial(mat);

				// set pos & scale 
				m_Cloth_Plane_->SetRotation(Quaternion(-90.0f, 0.0f, 0.0f));

				// save init world pos
				// HACK 0.46875 0 0.01
				Cloth_world_Center = Vector3(0.46875f, 0.0f, 0.01f);
				mv3_ClothPlaneInitPos_ = Cloth_world_Center;
				URHO3D_LOGINFO("	mv3_ClothPlaneInitPos_: " + mv3_ClothPlaneInitPos_.ToString());
				m_Cloth_Plane_->SetPosition(mv3_ClothPlaneInitPos_);

				Vector3 scale_vec = Vector3(plane_width, 0.0f, plane_height);
				m_Cloth_Plane_->SetScale(scale_vec);

				// save init scale
				mv3_ClothPlaneInitScale_ = m_Cloth_Plane_->GetScale();
				URHO3D_LOGINFO("	mv3_ClothPlaneInitScale_: " + mv3_ClothPlaneInitScale_.ToString());

				// mark a plane center
				{
					//// custom material with dissabled depth test
					//SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_green.xml")->Clone();
					//if (mat)
					//{
					//	mat->SetRenderOrder(200);   // higher render order
					//	SharedPtr<Technique> tec = mat->GetTechnique(0)->Clone();
					//	if (tec)
					//		tec->GetPass(0)->SetDepthTestMode(CMP_ALWAYS);   // Always pass depth test

					//	mat->SetTechnique(0, tec);
					//}

					//Node* mp_ClothCenterNode_ = scene_->CreateChild("ClothPlaneCenter_V");
					//StaticModel* box = mp_ClothCenterNode_->CreateComponent<StaticModel>();
					//box->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
					//box->SetMaterial(mat);

					//// set default pos & scale for visual scene object	
					//Cloth_world_Center.z_ = 0.0f;
					//mp_ClothCenterNode_->SetPosition(Cloth_world_Center);
					//mp_ClothCenterNode_->SetScale(Vector3(0.3f, 0.3f, 0.0f));
				}

				return true;
			}
		}
	}

	return false;
}

bool ClothEngine::ScaleClothTexture(Vector3 world_scale)
{
	if (!mp_ClothTex_ || !m_Cloth_Plane_)
	{
		URHO3D_LOGERROR("!!! ScaleClothTexture() -  mp_ClothTex_ || m_Cloth_Plane_ IS NULL !!!");
		return false;
	}

	m_Cloth_Plane_->Scale(world_scale);
	return true;
}

unsigned ClothEngine::GetClothMeasures(SharedPtr<MenpoPS>& target)
{
	if (!target & target->m_PROJ_MenpoPS_List.Size() < 49)
		return 0;

	URHO3D_LOGERROR("=== GetClothMeasures() ===");

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// custom material with dissabled depth test
	SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_mag.xml")->Clone();
	if (mat)
	{
		mat->SetRenderOrder(200);   // higher render order

		SharedPtr<Technique> tec = mat->GetTechnique(0)->Clone();
		if (tec)
			tec->GetPass(0)->SetDepthTestMode(CMP_ALWAYS);   // Always pass depth test

		mat->SetTechnique(0, tec);
	}

	// neck
	{
		mpr_ClothNeck_.first_ = Vector3(target->m_PROJ_MenpoPS_List.At(5).second_, 0.0f);
		mpr_ClothNeck_.second_ = Vector3(target->m_PROJ_MenpoPS_List.At(12).second_, 0.0f);

		v3_LErpedNeck_ = VectorLerp(mpr_ClothNeck_.first_, mpr_ClothNeck_.second_, Vector3(0.5f, 0.5f, 0.5f));

		mf_NeckDist_.first_ = 0.0f;
		mf_NeckDist_.second_ = fabsf(SegLenght(mpr_ClothNeck_.first_, mpr_ClothNeck_.second_));

		// node
		{
			Node* pNeckNode = scene_->CreateChild("NP_N");
			StaticModel* sphere = pNeckNode->CreateComponent<StaticModel>();
			sphere->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
			sphere->SetMaterial(mat);

			pNeckNode->SetPosition(v3_LErpedNeck_);
			pNeckNode->SetScale(Vector3(0.2f, 0.2f, 0.0f));

			Node* TitleNode = pNeckNode->CreateChild();
			TitleNode->SetName("NP_NT");
			TitleNode->SetPosition(Vector3(0.0f, 1.2f, 0.0f));
			TitleNode->SetScale(Vector3(4.0f, 4.0f, 0.0f));
			Text3D* TitleText = TitleNode->CreateComponent<Text3D>();
			TitleText->SetText(String(mf_NeckDist_.second_));
			TitleText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.sdf"), 24);
			TitleText->SetFontSize(50);

			TitleText->SetColor(Color::WHITE);
			//TitleText->SetTextEffect(TE_SHADOW);
			TitleText->SetEffectColor(Color(0.5f, 0.5f, 0.5f));
			TitleText->SetAlignment(HA_CENTER, VA_CENTER);
		}
	}

	// shoulders
	{
		mpr_ClothShoulders_.first_ = Vector3(target->m_PROJ_MenpoPS_List.At(4).second_, 0.0f);
		mpr_ClothShoulders_.second_ = Vector3(target->m_PROJ_MenpoPS_List.At(13).second_, 0.0f);

		v3_LErpedShoulders_ = VectorLerp(mpr_ClothShoulders_.first_, mpr_ClothShoulders_.second_, Vector3(0.5f, 0.5f, 0.5f));
		mf_ShouldersDist_.first_ = 0.0f;
		mf_ShouldersDist_.second_ = fabsf(SegLenght(mpr_ClothShoulders_.first_, mpr_ClothShoulders_.second_));

		// node
		{
			Node* pShouldersNode = scene_->CreateChild("NP_S");
			StaticModel* sphere = pShouldersNode->CreateComponent<StaticModel>();
			sphere->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
			sphere->SetMaterial(mat);

			pShouldersNode->SetPosition(v3_LErpedShoulders_);
			pShouldersNode->SetScale(Vector3(0.2f, 0.2f, 0.0f));

			Node* TitleNode = pShouldersNode->CreateChild();
			TitleNode->SetName("NP_ST");
			TitleNode->SetPosition(Vector3(0.0f, 1.2f, 0.0f));
			TitleNode->SetScale(Vector3(4.0f, 4.0f, 0.0f));
			Text3D* TitleText = TitleNode->CreateComponent<Text3D>();
			TitleText->SetText(String(mf_ShouldersDist_.second_));
			TitleText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.sdf"), 24);
			TitleText->SetFontSize(50);

			TitleText->SetColor(Color::WHITE);
			//TitleText->SetTextEffect(TE_SHADOW);
			TitleText->SetEffectColor(Color(0.5f, 0.5f, 0.5f));
			TitleText->SetAlignment(HA_CENTER, VA_CENTER);
		}
	}

	// waist
	{
		mpr_ClothWaist_.first_ = Vector3(target->m_PROJ_MenpoPS_List.At(42).second_, 0.0f);
		mpr_ClothWaist_.second_ = Vector3(target->m_PROJ_MenpoPS_List.At(24).second_, 0.0f);

		v3_LErpedWaist_ = VectorLerp(mpr_ClothWaist_.first_, mpr_ClothWaist_.second_, Vector3(0.5f, 0.5f, 0.5f));
		mf_WaistDist_.first_ = 0.0f;
		mf_WaistDist_.second_ = fabsf(SegLenght(mpr_ClothWaist_.first_, mpr_ClothWaist_.second_));

		// node
		{
			Node* pWaistNode = scene_->CreateChild("NP_W");
			StaticModel* sphere = pWaistNode->CreateComponent<StaticModel>();
			sphere->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
			sphere->SetMaterial(mat);

			pWaistNode->SetPosition(v3_LErpedWaist_);
			pWaistNode->SetScale(Vector3(0.2f, 0.2f, 0.0f));

			Node* TitleNode = pWaistNode->CreateChild();
			TitleNode->SetName("NP_WT");
			TitleNode->SetPosition(Vector3(0.0f, 1.2f, 0.0f));
			TitleNode->SetScale(Vector3(4.0f, 4.0f, 0.0f));
			Text3D* TitleText = TitleNode->CreateComponent<Text3D>();
			TitleText->SetText(String(mf_WaistDist_.second_));
			TitleText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.sdf"), 24);
			TitleText->SetFontSize(50);

			TitleText->SetColor(Color::WHITE);
			//TitleText->SetTextEffect(TE_SHADOW);
			TitleText->SetEffectColor(Color(0.5f, 0.5f, 0.5f));
			TitleText->SetAlignment(HA_CENTER, VA_CENTER);
		}
	}

	// croch
	{
		mpr_ClothCroch_.first_ = Vector3(target->m_PROJ_MenpoPS_List.At(41).second_, 0.0f);
		mpr_ClothCroch_.second_ = Vector3(target->m_PROJ_MenpoPS_List.At(25).second_, 0.0f);

		v3_LErpedCroch_ = VectorLerp(mpr_ClothCroch_.first_, mpr_ClothCroch_.second_, Vector3(0.5f, 0.5f, 0.5f));
		mf_CrochDist_.first_ = 0.0f;
		mf_CrochDist_.second_ = fabsf(SegLenght(mpr_ClothCroch_.first_, mpr_ClothCroch_.second_));

		// node
		{
			Node* pCrochNode = scene_->CreateChild("NP_C");
			StaticModel* sphere = pCrochNode->CreateComponent<StaticModel>();
			sphere->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
			sphere->SetMaterial(mat);

			pCrochNode->SetPosition(v3_LErpedCroch_);
			pCrochNode->SetScale(Vector3(0.2f, 0.2f, 0.0f));

			Node* TitleNode = pCrochNode->CreateChild();
			TitleNode->SetName("NP_WT");
			TitleNode->SetPosition(Vector3(0.0f, 1.2f, 0.0f));
			TitleNode->SetScale(Vector3(4.0f, 4.0f, 0.0f));
			Text3D* TitleText = TitleNode->CreateComponent<Text3D>();
			TitleText->SetText(String(mf_CrochDist_.second_));
			TitleText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.sdf"), 24);
			TitleText->SetFontSize(50);

			TitleText->SetColor(Color::WHITE);
			//TitleText->SetTextEffect(TE_SHADOW);
			TitleText->SetEffectColor(Color(0.5f, 0.5f, 0.5f));
			TitleText->SetAlignment(HA_CENTER, VA_CENTER);
		}
	}

	// l_hand
	{
		mpr_ClothLHand_.first_ = Vector3(target->m_PROJ_MenpoPS_List.At(2).second_, 0.0f);
		mpr_ClothLHand_.second_ = Vector3(target->m_PROJ_MenpoPS_List.At(45).second_, 0.0f);

		v3_LErpedLHand_ = VectorLerp(mpr_ClothLHand_.first_, mpr_ClothLHand_.second_, Vector3(0.5f, 0.5f, 0.5f));
		mf_LHandDist_.first_ = 0.0f;
		mf_LHandDist_.second_ = fabsf(SegLenght(mpr_ClothLHand_.first_, mpr_ClothLHand_.second_));
	}

	// r_hand
	{
		mpr_ClothRHand_.first_ = Vector3(target->m_PROJ_MenpoPS_List.At(21).second_, 0.0f);
		mpr_ClothRHand_.second_ = Vector3(target->m_PROJ_MenpoPS_List.At(15).second_, 0.0f);

		v3_LErpedRHand_ = VectorLerp(mpr_ClothRHand_.first_, mpr_ClothRHand_.second_, Vector3(0.5f, 0.5f, 0.5f));
		mf_RHandDist_.first_ = 0.0f;
		mf_RHandDist_.second_ = fabsf(SegLenght(mpr_ClothRHand_.first_, mpr_ClothRHand_.second_));
	}

	// MPS-49 neck-croch height
	{
		mf_NeckToCrochHeight_ = v3_LErpedNeck_.y_ - v3_LErpedCroch_.y_;
		v3_NeckToCrochCenter_ = VectorLerp(v3_LErpedNeck_, v3_LErpedCroch_, Vector3(0.5f, 0.5f, 0.5f));
		URHO3D_LOGINFO("=> mf_ShouldersDist_ = " + String(mf_ShouldersDist_.second_) + " | mf_NeckToCrochHeight_ = " + String(mf_NeckToCrochHeight_));

		// node
		{
			Node* pCCNode = scene_->CreateChild("NP_NH");
			//StaticModel* sphere = pCCNode->CreateComponent<StaticModel>();
			//sphere->SetModel(cache->GetResource<Model>("Models/Sphere.mdl"));
			//sphere->SetMaterial(mat);

			pCCNode->SetPosition(v3_NeckToCrochCenter_);
			pCCNode->SetScale(Vector3(0.2f, 0.2f, 0.0f));

			Node* TitleNode = pCCNode->CreateChild();
			TitleNode->SetName("NP_NHT");
			TitleNode->SetPosition(Vector3(0.0f, 1.2f, 0.0f));
			TitleNode->SetScale(Vector3(4.0f, 4.0f, 0.0f));
			Text3D* TitleText = TitleNode->CreateComponent<Text3D>();
			TitleText->SetText(String(mf_NeckToCrochHeight_));
			TitleText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.sdf"), 24);
			TitleText->SetFontSize(50);

			TitleText->SetColor(Color::WHITE);
			//TitleText->SetTextEffect(TE_SHADOW);
			TitleText->SetEffectColor(Color(0.5f, 0.5f, 0.5f));
			TitleText->SetAlignment(HA_CENTER, VA_CENTER);
		}
	}

	mb_drawClothContour_ = true;

	return 1;
}

bool ClothEngine::MoveClothTexture(/*Vector2 screen_v,*/ Vector3 world_v)
{
	URHO3D_LOGINFO("=== MoveClothTexture() | world_v: " + world_v.ToString() + "===");

	m_Cloth_Plane_->Translate(world_v);
	return true;
}


unsigned ClothEngine::LoadClothTextuteContour(const String& file_name, bool attached, bool draw)
{
	URHO3D_LOGINFO("=== LoadClothTextuteContour() | file_name: " + file_name + " ===");

	// clear cloth tex contour
	mv_ClothTextureContour_.Clear();
	mv_ClothTextureContourClone_.Clear();

//	String sys_path = GetSubsystem<FileSystem>()->GetProgramDir();
	String sys_path = "";
	String full_path = sys_path + file_name;


	File loadFile(context_, full_path, FILE_READ);
	SharedPtr<XMLFile> xml(new XMLFile(context_));
	if (xml->Load(loadFile))
	{
		XMLElement hull_elem = xml->GetRoot("Hull");
		unsigned size = hull_elem.GetUInt("size");

		if (size > 0)
		{
			// clear cloth tex contour
			mv_ClothTextureContour_.Clear();
			mv_ClothTextureContourClone_.Clear();

			for (unsigned i = 0u; i < size; ++i)
			{
				XMLElement p_elem = hull_elem.GetChild("P" + String(i));
				Pair<IntVector2, Node*> contour_pair;
				contour_pair.first_ = p_elem.GetIntVector2("screen");
				contour_pair.second_ = nullptr;

				// push to contour vector
				mv_ClothTextureContour_.Push(contour_pair);

				Vector3 w_pos = p_elem.GetVector3("world");
				mv_ClothTextureContourClone_.Push(w_pos);
			}
		}

		
		// get MEASURED dists
		RecomputeContourMeasures(false);

		if (mv_ClothTextureContour_.Size() == mv_ClothTextureContourClone_.Size())
			return mv_ClothTextureContour_.Size();
	}

	return 0;
}

void ClothEngine::VizClothTextuteContour(bool attached)
{
	if (mv_ClothTextureContour_.Size() < 80)
		return;

	if (mv_CLOTH_MESH_POS_.Empty())
		return;

	URHO3D_LOGINFO("=== VizClothTextuteContour() ===");
	// create contour CPs
	{
		// global cache
		ResourceCache* cache = GetSubsystem<ResourceCache>();
		// custom material with dissabled depth test
		SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_red.xml")->Clone();
		if (mat)
		{
			mat->SetRenderOrder(200);   // higher render order
			SharedPtr<Technique> tec = mat->GetTechnique(0)->Clone();
			if (tec)
				tec->GetPass(0)->SetDepthTestMode(CMP_ALWAYS);   // Always pass depth test

			mat->SetTechnique(0, tec);
		}

		// creation loop {red points}
		for (unsigned c = 0; c < vpr_ContourMeshCorresponded_.Size(); ++c)
		{
			mv_ClothTextureContour_[c].second_ = scene_->CreateChild("LCP" + String(c));

			StaticModel* box = mv_ClothTextureContour_[c].second_->CreateComponent<StaticModel>();
			box->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
			box->SetMaterial(mat);

			if (_LOGGING_)
				URHO3D_LOGINFO("-> node mv_ClothTextureContour_[" + String(c) + "]: " + mv_ClothTextureContourClone_[c].ToString());

			Vector3 cw_pos = mv_CLOTH_MESH_POS_.At(vpr_ContourMeshCorresponded_[c].second_);
			mv_ClothTextureContour_[c].second_->SetPosition(cw_pos);

			if (attached)
			{
				mv_ClothTextureContour_[c].second_->SetParent(m_Cloth_Plane_);
				mv_ClothTextureContour_[c].second_->SetScale(Vector3(0.02f, 0.02f, 0.0f));
			}
			else
			{
				mv_ClothTextureContour_[c].second_->SetScale(Vector3(0.1f, 0.1f, 0.0f));
			}
		}

		// compute contour geom center
		mpr_ClothContourCenter_.first_ = Vector3::ZERO;
		{
			for (auto i : mv_ClothTextureContour_)
				mpr_ClothContourCenter_.first_ += i.second_->GetWorldPosition();

			mpr_ClothContourCenter_.first_ = mpr_ClothContourCenter_.first_ / mv_ClothTextureContour_.Size();
			URHO3D_LOGINFO("-> CLOTH CONTOUR CENTER mv3_ClothContourCenter_: " + mpr_ClothContourCenter_.first_.ToString());
		}

	}
}

bool ClothEngine::PickClothTextuteContour(const IntVector2 target_pixel)
{
	/*if (!m_Cloth_Plane_ || !mp_ClothTex_)
	return false;*/

	URHO3D_LOGINFO("=== PickClothTextuteContour() | target_pixel - " + target_pixel.ToString() + " ===");

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// custom material with dissabled depth test
	SharedPtr<Material> mat = cache->GetResource<Material>("Materials/VColUnlit_red.xml")->Clone();
	if (mat)
	{
		mat->SetRenderOrder(200);   // higher render order
		SharedPtr<Technique> tec = mat->GetTechnique(0)->Clone();
		if (tec)
			tec->GetPass(0)->SetDepthTestMode(CMP_ALWAYS);   // Always pass depth test

		mat->SetTechnique(0, tec);
	}

	// check if already exist
	bool exist = false;
	for (unsigned i = 0; i < mv_ClothTextureContour_.Size(); ++i)
	{
		if (mv_ClothTextureContour_[i].first_ == target_pixel)
		{
			URHO3D_LOGINFO("-> target_pixel IS ALREADY EXIST");
			if (!mv_ClothTextureContour_[i].second_)
			{
				URHO3D_LOGINFO("-> target_pixel Node IS NULL - creation...");

				mv_ClothTextureContour_[i].second_ = scene_->CreateChild("C" + String(i));
				StaticModel* box = mv_ClothTextureContour_[i].second_->CreateComponent<StaticModel>();
				box->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
				box->SetMaterial(mat);

				// project screen pos to world pos
				// Get world coords for visual plane
				Vector3 world_pos = Vector3::ZERO;
				{
					Camera* camera = cameraNode_->GetComponent<Camera>();
					if (camera)
					{
						Renderer* p_render = GetSubsystem<Renderer>();
						if (p_render)
						{
							Viewport* p_view = p_render->GetViewport(0);
							if (p_view)
							{
								world_pos = p_view->ScreenToWorldPoint(target_pixel.x_, target_pixel.y_, mf_Z_Dist_);
								world_pos.z_ = 0.001f;
							}
						}
					}
				}

				mv_ClothTextureContour_[i].second_->SetPosition(world_pos);
				mv_ClothTextureContour_[i].second_->SetScale(Vector3(0.1f, 0.1f, 0.0f));
			}

			exist = true;
			break;
		}
	}

	if (exist)
		return false;
	else
	{
		URHO3D_LOGINFO("-> CREATE NEW target_pixel...");

		// create one
		Pair<IntVector2, Node*> res;
		res.first_ = target_pixel;

		// create vis node
		res.second_ = scene_->CreateChild("C" + String(mv_ClothTextureContour_.Size()));
		StaticModel* box = res.second_->CreateComponent<StaticModel>();
		box->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
		box->SetMaterial(mat);

		// project screen pos to world pos
		// Get world coords for visual plane
		Vector3 world_pos = Vector3::ZERO;
		{
			Camera* camera = cameraNode_->GetComponent<Camera>();
			if (camera)
			{
				Renderer* p_render = GetSubsystem<Renderer>();
				if (p_render)
				{
					Viewport* p_view = p_render->GetViewport(0);
					if (p_view)
					{
						world_pos = p_view->ScreenToWorldPoint(target_pixel.x_, target_pixel.y_, mf_Z_Dist_);
						world_pos.z_ = 0.0f;
					}
				}
			}
		}

		URHO3D_LOGINFO("target_pixel - SCREEN: " + target_pixel.ToString() + " | WORLD: " + world_pos.ToString());

		res.second_->SetPosition(world_pos);
		res.second_->SetScale(Vector3(0.1f, 0.1f, 0.0f));

		mv_ClothTextureContour_.Push(res);
		mv_ClothTextureContourClone_.Push(world_pos);

		URHO3D_LOGINFO("	mv_ClothTextureContour_: " + String(mv_ClothTextureContour_.Size()) +
			" | mv_ClothTextureContourClone_: " + String(mv_ClothTextureContourClone_.Size()));
	}
}

bool ClothEngine::SaveClothTextuteContour(const String& path)
{
	/*if (mv_ClothTextureContour_.Empty() || mv_ClothTextureContourClone_.Empty())
	return false;

	if (mv_ClothTextureContour_.Size() != mv_ClothTextureContourClone_.Size())
	{
	URHO3D_LOGERROR("!!! mv_ClothTextureContour_.Size() != m_ClothTextureContourClone_.Size() !!!");
	return false;
	}*/

	URHO3D_LOGINFO("=== SaveClothTextuteContour() | file_name: " + path + " ===");

	String sys_path = GetSubsystem<FileSystem>()->GetProgramDir();
	//String file_name = "contour_1" + String(mu_Cloth_Idx_);

	std::freopen(path.CString(), "w", stdout);

	// save to text file
	for (unsigned i = 0; i < mv_ClothTextureContourClone_.Size(); ++i)
	{
		Vector3 w_pos = mv_ClothTextureContour_[i].second_->GetWorldPosition();
		Vector2 w_pos_write = Vector2(w_pos.x_, w_pos.y_);
		//URHO3D_LOGINFO("=> SAVE w_pos P[" + String(i) + "]:" + w_pos.ToString());

		// write to file
		std::cout << w_pos_write.ToString().CString() << std::endl;
	}

	fclose(stdout);
	//return true;



	File saveFile(context_, (sys_path + path) + ".xml", FILE_WRITE);
	SharedPtr<XMLFile> xml(new XMLFile(context_));

	XMLElement root = xml->CreateRoot("Hull");
	root.SetUInt("size", mv_ClothTextureContourClone_.Size());

	for (unsigned i = 0; i < mv_ClothTextureContourClone_.Size(); ++i)
	{
		XMLElement screen_elem = root.CreateChild("P" + String(i));
		screen_elem.SetIntVector2("screen", mv_ClothTextureContour_[i].first_);

		screen_elem.SetVector3("world", mv_ClothTextureContourClone_[i]);
	}

	if (xml->Save(saveFile))
		return true;
	else
		URHO3D_LOGERROR("!!! Cannot create " + path + " FILE !!!");

	return true;
}

bool ClothEngine::RecomputeContourMeasures(bool scene_nodes)
{
	if (mv_ClothTextureContourClone_.Empty() || mv_ClothTextureContour_.Empty())
		return false;

	URHO3D_LOGINFO("=== RecomputeContourMeasures() | scene_nodes - " + String(scene_nodes) + "===");

	if (!scene_nodes)
	{
		v3_ContourShouldersL = mv_ClothTextureContourClone_.At(51);
		v3_ContourShouldersR = mv_ClothTextureContourClone_.At(34);
	}
	else
	{
		v3_ContourShouldersL = mv_CLOTH_MESH_POS_.At(vpr_ContourMeshCorresponded_.At(51).second_);
		v3_ContourShouldersR = mv_CLOTH_MESH_POS_.At(vpr_ContourMeshCorresponded_.At(34).second_);
	}

	v3_ContourshouldersCenter = VectorLerp(v3_ContourShouldersL, v3_ContourShouldersR, Vector3(0.5f, 0.5f, 0.5f));
	mf_ClothWidth_ = fabsf(SegLenght(v3_ContourShouldersL, v3_ContourShouldersR));

	if (!scene_nodes)
	{
		v3_ContourCrochL = mv_ClothTextureContourClone_.At(0);
		v3_ContourCrochR = mv_ClothTextureContourClone_.At(9);
	}
	else
	{
		v3_ContourCrochL = mv_CLOTH_MESH_POS_.At(vpr_ContourMeshCorresponded_.At(0).second_);;
		v3_ContourCrochR = mv_CLOTH_MESH_POS_.At(vpr_ContourMeshCorresponded_.At(9).second_);;
	}

	v3_ContourCrochCenter = VectorLerp(v3_ContourCrochL, v3_ContourCrochR, Vector3(0.5f, 0.5f, 0.5f));
	mf_ClothHeight_ = fabsf(SegLenght(v3_ContourCrochCenter, v3_ContourshouldersCenter));

	URHO3D_LOGINFO("=> mf_ClothWidth_ = " + String(mf_ClothWidth_) + " | mf_ClothHeight_ = " + String(mf_ClothHeight_));

	// ====== COMPUTED W/H
	mf_ClothComputedWScaleX_ = mf_ShouldersDist_.second_ / mf_ClothWidth_;
	mf_ClothComputedHScaleY_ = mf_NeckToCrochHeight_ / mf_ClothHeight_;
	URHO3D_LOGINFO("=> COMPUTED mf_ClothComputedWScaleX_ = " + String(mf_ClothComputedWScaleX_) +
		" | mf_ClothComputedHScaleY_ = " + String(mf_ClothComputedHScaleY_));

	URHO3D_LOGINFO("=== END RecomputeContourMeasures() ===");

	return true;
}

void ClothEngine::DrawClothTexContour()
{
	if (mv_ClothTextureContourClone_.Empty() || mv_ClothTextureContour_.Empty())
		return;

	DebugRenderer* debug = scene_->GetComponent<DebugRenderer>();

	// contour
	{
		/*Vector3 start, end;
		if (mb_drawClothContour_)
		{
		for (unsigned p = 0; p < mv_ClothTextureContour_.Size(); ++p)
		{
		start = mv_ClothTextureContour_[p].second_->GetWorldPosition();
		end = mv_ClothTextureContour_[p + 1].second_->GetWorldPosition();

		debug->AddLine(start, end, Color::RED, true);
		}
		}*/
	}
	
	URHO3D_LOGINFO("-> mb_DrawClothMesh_ - "+String(mb_DrawClothMesh_)+"");
	// debug mesh
	if (mb_DrawClothMesh_)
	{
		Color col = Color::GREEN;
		for (unsigned p = 0; p < mv_CLOTH_MESH_INDICES_.Size(); p += 3)
		{
			unsigned Ai = mv_CLOTH_MESH_INDICES_.At(p);
			Vector3 A_ = mv_CLOTH_MESH_POS_.At(Ai);

			unsigned Bi = mv_CLOTH_MESH_INDICES_.At(p + 1);
			Vector3 B_ = mv_CLOTH_MESH_POS_.At(Bi);

			unsigned Ci = mv_CLOTH_MESH_INDICES_.At(p + 2);
			Vector3 C_ = mv_CLOTH_MESH_POS_.At(Ci);

			//start = mv_CLOTH_MESH_POS_[p];
			//end = mv_CLOTH_MESH_POS_[p + 1];

			debug->AddLine(A_, B_, col, false);
			debug->AddLine(B_, C_, col, false);
			debug->AddLine(C_, A_, col, false);

			//debug->AddTriangle(A_, B_, C_, Color::GRAY, false);
		}

		debug->AddLine(v3_ContourCrochL, v3_ContourCrochR, Color::GREEN, false);
		debug->AddLine(v3_ContourShouldersL, v3_ContourShouldersR, Color::GREEN, false);
	}
}


bool ClothEngine::LoadClothMesh(const String& file_name, const bool draw)
{
	if (file_name.Empty())
		return false;

	URHO3D_LOGINFO("=== LoadClothMesh() ===");

	//using namespace Eigen;

//	String sys_path = GetSubsystem<FileSystem>()->GetProgramDir();
	String sys_path = "";
	std::string sys_path_std = sys_path.CString();
	std::string f_name = sys_path_std + file_name.CString();

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// Urho loading
	{
		//if(!mp_ClothMeshNode_)
		//{
		//	mp_ClothMeshNode_ = scene_->CreateChild("CLOTH_MESH");

		//	//mp_ClothMeshNode_->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
		//	//modelNodeW->SetScale(Vector3(1.0f, 100.0f, 100.0f));
		//	StaticModel* ClothMeshModel = mp_ClothMeshNode_->CreateComponent<StaticModel>();

		//	URHO3D_LOGINFO("=> loading - "+ sys_path + file_name + ".mdl" +" ...");
		//	ClothMeshModel->SetModel(cache->GetResource<Model>(sys_path + file_name + ".mdl"));
		//	//ClothMeshModel->SetMaterial(cache->GetResource<Material>("Materials/DefaultGrey_green.xml"));
		//	ClothMeshModel->SetMaterial(cache->GetResource<Material>("Materials/GreenTransparent.xml"));

		//	mp_ClothModel_ = ClothMeshModel->GetModel();
		//	if (mp_ClothModel_)
		//	{
		//		mv3_ClothMeshGeomCenter_ = mp_ClothModel_->GetGeometryCenter(0);
		//		URHO3D_LOGINFO("=> CLOTH MODEL mv3_ClothMeshGeomCenter_ - " + mv3_ClothMeshGeomCenter_.ToString());
		//	}
		//	
		//	mp_ClothMeshNode_->SetPosition(mv3_ClothContourCenter_);

		//	//if(m_Cloth_Plane_)
		//		//mp_ClothMeshNode_->SetParent(m_Cloth_Plane_);


		//	//mp_ClothMeshNode_->SetScale(Vector3(0.0f, 0.0f, 0.0f));

		//	return true;
		//}
	}

	// IGL Loading
	{
		//f_name += ".obj";
		//f_name += ".off";
		f_name += "";
		
		// Triangulated interior
		Eigen::MatrixXd Triangulated_V2;
		Eigen::MatrixXi Triangulated_Faces;

		// LOW lvl LOADING
		{
			////std::string real_name = file_name.CString();
			//URHO3D_LOGINFO("=== LoadClothMesh() | file_name - "+ file_name +"===");

			//unsigned lines_count = 0;
			//std::string line_v;

			//std::string nv = (real_name + "_v").c_str();
			//std::ifstream file_v(nv.c_str());
			//if (file_v.is_open())
			//{
			//	URHO3D_LOGINFO("=> loading vertices - "+ String(nv.c_str()));
			//	mv_CLOTH_MESH_POS_.Clear();

			//	while (file_v.good())
			//	{
			//		getline(file_v, line_v);
			//		String r_nv = nv.c_str();

			//		//unsigned elements = CountElements_v2(r_nv.CString(), ' ');
			//		//if (elements < 2)
			//		//{
			//		//	URHO3D_LOGINFO("!!! elements < 2 in readed line !!!");
			//		//	//cout << "[ERROR]: elements < 2 in readed line !!!" << endl;
			//		//	//break;
			//		//}

			//		/*char* ptr = (char*)line.c_str();

			//		Pair<float, float> res_v2;
			//		res_v2.first_ = (float)strtod(ptr, &ptr);
			//		res_v2.second_ = (float)strtod(ptr, &ptr);*/

			//		//String v_line = line.c_str();
			//		//URHO3D_LOGINFO("-> line["+String(lines_count)+"]: "+ v_line.CString());
			//		Vector2 val_pos = ToVector2(line_v.c_str());

			//		//mv_CLOTH_MESH_POS_.Push(Vector3(res_v2.first_, res_v2.second_, 0.0f));
			//		mv_CLOTH_MESH_POS_.Push(Vector3(val_pos, 0.0f));
			//		++lines_count;
			//	}

			//	file_v.close();
			//}



			//// indices
			//lines_count = 0;
			//std::string line_f;

			//std::string nf = (real_name + "_f").c_str();
			//std::ifstream file_f(nf.c_str());
			//if (file_f.is_open())
			//{
			//	URHO3D_LOGINFO("=> loading indices - " + String(nf.c_str()));
			//	mv_CLOTH_MESH_INDICES_.Clear();

			//	while (file_f.good())
			//	{
			//		getline(file_f, line_v);
			//		String r_nf = nf.c_str();

			//		//unsigned elements = CountElements_v2(r_nf.CString(), ' ');
			//		//if (elements < 2)
			//		//{
			//		//	URHO3D_LOGINFO("!!! elements < 2 in readed line !!!");
			//		//	//cout << "[ERROR]: elements < 2 in readed line !!!" << endl;
			//		//	//break;
			//		//}

			//		/*char* ptr = (char*)line.c_str();

			//		Pair<float, float> res_v2;
			//		res_v2.first_ = (float)strtod(ptr, &ptr);
			//		res_v2.second_ = (float)strtod(ptr, &ptr);*/

			//		//String rf_line = line.c_str();
			//		//IntVector3 f_tri = ToIntVector3(line.c_str());

			//		Vector3 f_tri = ToVector3(line_v.c_str());

			//		mv_CLOTH_MESH_INDICES_.Push(static_cast<unsigned>(f_tri.x_));
			//		mv_CLOTH_MESH_INDICES_.Push(static_cast<unsigned>(f_tri.y_));
			//		mv_CLOTH_MESH_INDICES_.Push(static_cast<unsigned>(f_tri.z_));
			//		++lines_count;
			//	}

			//	file_f.close();
			//}

			//// DEBUG
			//URHO3D_LOGINFO("=> DEBUG VERTICES - "+String(mv_CLOTH_MESH_POS_.Size())+"");
			///*for (unsigned pos = 0; pos < mv_CLOTH_MESH_POS_.Size(); ++pos)
			//	URHO3D_LOGINFO("	V["+String(pos)+"]: "+ mv_CLOTH_MESH_POS_[pos].ToString());*/

			//URHO3D_LOGINFO("=> DEBUG INDICES - "+String(mv_CLOTH_MESH_INDICES_.Size())+"");
			//for (unsigned pos = 0; pos < mv_CLOTH_MESH_INDICES_.Size(); ++pos)
			//{
			//	URHO3D_LOGINFO("	F[" + String(pos) + "]: " + String( mv_CLOTH_MESH_POS_[pos])/* + " "+String(mv_CLOTH_MESH_POS_[pos + 1])+
			//		 " "+String(mv_CLOTH_MESH_POS_[pos + 2])*/);
			//}
		}

		// LOADER CODE
		{
			//freopen("D_CLOTH_MESH", "a", stdout);
			//URHO3D_LOGINFO("=> START loading mesh: " + String(f_name.c_str()));

			//if (igl::readOFF(f_name, Triangulated_V2, Triangulated_Faces))
			if (igl::readOBJ(f_name, Triangulated_V2, Triangulated_Faces))
			{
				URHO3D_LOGINFO("=> loaded mesh - " + String(f_name.c_str()));

				mv_CLOTH_MESH_POS_.Clear();
				unsigned v_size = static_cast<unsigned>(Triangulated_V2.rows());
				mv_CLOTH_MESH_POS_.Reserve(v_size);

				URHO3D_LOGINFO("=> VERTICES: " + String(v_size));

				double DX = 0.0;
				double DY = 0.0;
				double DZ = 0.0;

				for (int i = 0; i < v_size; ++i)
				{
					DX = Triangulated_V2.row(i).x();
					DY = Triangulated_V2.row(i).y();

					{
						//URHO3D_LOGERROR("	DX: "+String(DX)+" | DY: "+String(DY));

						//if (DX == 0.0 && DY == 0.0)
						//{
						//	URHO3D_LOGERROR("!!! DX && DY == 0.0f - END OF vector !!!");
						//	//std::cout << "===> DX && DY == 0.0f - END OF vector..." << std::endl;
						//	//continue;
						//	break;
						//}
					}
				
					//mv_CLOTH_MESH_POS_.Push(Vector3(DX, DY, DZ));
					mv_CLOTH_MESH_POS_.Push(std::move(Vector3(DX, DY, DZ)));
				}

				// debug mv_CLOTH_MESH_POS_			
				{
					//URHO3D_LOGINFO("=> mv_CLOTH_MESH_POS_ - " + String(mv_CLOTH_MESH_POS_.Size()));

					/*	for (int i = 0; i < mv_CLOTH_MESH_POS_.Size(); ++i)
					URHO3D_LOGINFO("P[" +String(i)+ "]: "+String(mv_CLOTH_MESH_POS_[i].ToString()));*/
				}


				// === EDGES ===
				mv_CLOTH_MESH_INDICES_.Clear();
				unsigned fr_size = static_cast<unsigned>(Triangulated_Faces.rows());
				unsigned fc_size = static_cast<unsigned>(Triangulated_Faces.cols());

				mv_CLOTH_MESH_INDICES_.Reserve(fr_size * fc_size);
				URHO3D_LOGINFO("faces rows: " + String(fr_size) + "  |  cols: " + String(fc_size));

				unsigned FX = 0;
				unsigned FY = 0;
				unsigned FZ = 0;

				for (unsigned i = 0; i < fr_size; ++i)
				{
					FX = Triangulated_Faces.row(i).x();
					FY = Triangulated_Faces.row(i).y();
					FZ = Triangulated_Faces.row(i).z();

					//URHO3D_LOGERROR("face["+String(i)+"]:	FX: "+String(FX)+" | FY: "+String(FY)+ " | FZ: "+String(FZ));

					mv_CLOTH_MESH_INDICES_.Push(FX);
					mv_CLOTH_MESH_INDICES_.Push(FY);
					mv_CLOTH_MESH_INDICES_.Push(FZ);
				}

				//URHO3D_LOGINFO("=> mv_CLOTH_MESH_INDICES_ - " + String(mv_CLOTH_MESH_INDICES_.Size()));

				if (!mv_CLOTH_MESH_POS_.Empty() && !mv_CLOTH_MESH_INDICES_.Empty())
				{
					// compare/find corresponded cloth CPs in loaded mesh
					//unsigned cp_finded = CheckContourMeshCompare();

					//("=== END LoadClothMesh() | cp_finded: "+String(cp_finded)+" ===");

					// close debug file
					//fclose(stdout);

					return true;
				}
			}
		}
	}


	mb_DrawClothMesh_ = draw;

	return false;
}

bool ClothEngine::FormClothMesh(const String& file_name)
{
	URHO3D_LOGINFO("=== FormClothMesh | V: "+String(mv_CLOTH_MESH_POS_.Size())+
		" | I: "+String(mv_CLOTH_MESH_INDICES_.Size())+" | UV: "+String(mv_ClothUVs_.Size())+"===");

	// form model inst
	SharedPtr<Model> ClothModel(new Model(context_));
	Vector<PODVector<unsigned> > allBoneMappings;
	BoundingBox box;

	SharedPtr<IndexBuffer> ib;
	SharedPtr<VertexBuffer> vb;
	Vector<SharedPtr<VertexBuffer> > vbVector;
	Vector<SharedPtr<IndexBuffer> > ibVector;
	unsigned startVertexOffset = 0;
	unsigned startIndexOffset = 0;
	unsigned destGeomIndex = 0;

	ClothModel->SetNumGeometries(1);

	PODVector<VertexElement> elements = GetVertexElements();

	URHO3D_LOGINFO("...creating new buffers...");
	{
		vb = new VertexBuffer(context_);
		ib = new IndexBuffer(context_);
		vb->SetShadowed(true);
		ib->SetShadowed(true);

		vb->SetSize(mv_CLOTH_MESH_POS_.Size(), elements, true);
		ib->SetSize(mv_CLOTH_MESH_INDICES_.Size(), false, true);
		
		URHO3D_LOGINFO("ib ishadowed - " + String(ib->IsShadowed()) + "");
		URHO3D_LOGINFO("vb ishadowed - " + String(vb->IsShadowed()) + "");

		vbVector.Push(vb);
		ibVector.Push(ib);
		startVertexOffset = 0;
		startIndexOffset = 0;
	}
	
	// initial transformations
	Matrix3x4 vertexTransform;
	Matrix3 normalTransform;
	Vector3 pos, scale;
	Quaternion rot;

	{
		//vertexTransform = Matrix3x4(pos, rot, scale);
		vertexTransform = Matrix3x4::IDENTITY;
		normalTransform = rot.RotationMatrix();
		URHO3D_LOGINFO("VERTEX TRANSFORM: - " + vertexTransform.ToString() + "");
		URHO3D_LOGINFO("pos: " + pos.ToString() + "");
		URHO3D_LOGINFO("scale: " + scale.ToString() + "");
		URHO3D_LOGINFO("rot: " + rot.ToString() + "");
		URHO3D_LOGINFO("--------------------------------------------------");
		URHO3D_LOGINFO("normalTransform: " + vertexTransform.ToString() + "");
		URHO3D_LOGINFO("--------------------------------------------------");
	}
	
	// geometry inst
	SharedPtr<Geometry> geom(new Geometry(context_));
	{
		if (geom)
			URHO3D_LOGINFO("... Creating geometry instance...");
		else
		{
			URHO3D_LOGERROR("!!! Creating geometry instance FAILED !!!");
			return false;
		}			
	}
	

	// Write index data
	URHO3D_LOGINFO("... Write index data...");
	unsigned char* indexData = ib->GetShadowData();
	unsigned short* dest_i = (unsigned short*)indexData + startIndexOffset;
	unsigned faces_cnt(0u);
	{
		if (!indexData)
		{
			URHO3D_LOGERROR("INDEX DATA - IS NULL");
			return false;
		}

		if (!dest_i)
		{
			URHO3D_LOGERROR("INDEX DEST - IS NULL");
			return false;
		}

		unsigned faces = mv_CLOTH_MESH_INDICES_.Size() / 3;
		for (unsigned j = 0; j < mv_CLOTH_MESH_INDICES_.Size(); j += 3)
		{
			*dest_i++ = mv_CLOTH_MESH_INDICES_[j]; + startVertexOffset;
			*dest_i++ = mv_CLOTH_MESH_INDICES_[j + 1]; + startVertexOffset;
			*dest_i++ = mv_CLOTH_MESH_INDICES_[j + 2]; + startVertexOffset;
			++faces_cnt;
		}

		URHO3D_LOGINFO("...faces wroted: " + String(faces_cnt) + ", orig faces in mesh: "
			+ String(faces));
	}


	// Write the vertex data
	URHO3D_LOGINFO("... Write vertex data...");
	unsigned char* vertexData = vb->GetShadowData();
	float* dest_v = (float*)((unsigned char*)vertexData + startVertexOffset * vb->GetVertexSize());
	unsigned vertices_cnt(0u);
	{
		for (unsigned j = 0; j < mv_CLOTH_MESH_POS_.Size(); ++j)
		{
			Vector3 vertex = vertexTransform * mv_CLOTH_MESH_POS_[j];
			box.Merge(vertex);
			*dest_v++ = vertex.x_;
			*dest_v++ = vertex.y_;
			*dest_v++ = vertex.z_;

			if (mv_ClothUVs_.Size() > 0u)
			{
				Vector2 uv = mv_ClothUVs_[j];
				*dest_v++ = uv.x_;
				*dest_v++ = uv.y_;
			}

			++vertices_cnt;
		}
	}

	URHO3D_LOGINFO("...verts wroted: " + String(vertices_cnt) + ", orig verts in mesh: "
		+ String(mv_CLOTH_MESH_POS_.Size()) + "");

	// Calculate the geometry center
	Vector3 center = Vector3::ZERO;
	{
		for (unsigned j = 0; j < mv_CLOTH_MESH_POS_.Size(); ++j)
			center += vertexTransform * mv_CLOTH_MESH_POS_[j];
	}	

	center /= static_cast<float>(mv_CLOTH_MESH_POS_.Size());
	
	URHO3D_LOGINFO("geom centr -  " + center.ToString() + "");
	
	// Define the geometry
	geom->SetIndexBuffer(ib);
	geom->SetVertexBuffer(0, vb);
	geom->SetDrawRange(TRIANGLE_LIST, startIndexOffset, mv_CLOTH_MESH_INDICES_.Size(), true);
	ClothModel->SetNumGeometryLodLevels(destGeomIndex, 1);
	ClothModel->SetGeometry(destGeomIndex, 0, geom);
	ClothModel->SetGeometryCenter(destGeomIndex, center);

	// Define the model buffers and bounding box
	PODVector<unsigned> emptyMorphRange;
	ClothModel->SetVertexBuffers(vbVector, emptyMorphRange, emptyMorphRange);
	ClothModel->SetIndexBuffers(ibVector);
	ClothModel->SetBoundingBox(box);


	File outFile(context_);
	if (!outFile.Open(file_name, FILE_WRITE))
	{
		URHO3D_LOGERROR(" !!! CANNOT OPEN FILE - " + file_name + "");
		return false;
	}

	if (ClothModel->Save(outFile))
	{
		URHO3D_LOGINFO(" !!! SUCCES SAVING FILE !!!");
		return true;
	}
	else
	{
		URHO3D_LOGERROR(" !!! FAILED SAVING FILE !!!");
		return false;
	}
}


bool ClothEngine::LoadFormedClothMesh(const String& path)
{
	if (path.Empty())
		return false;

	URHO3D_LOGINFO("=== LoadClothMesh() | path - "+ path +" ===");

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	mp_Cloth_Node_ = scene_->CreateChild("Cloth");
	mp_Cloth_Node_->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	StaticModel* test_model = mp_Cloth_Node_->CreateComponent<StaticModel>();
	test_model->SetModel(cache->GetResource<Model>(path));

	//Model* pModel = test_model->GetModel();
	//unsigned geometries_count = pModel->GetNumGeometries();

	test_model->SetMaterial(cache->GetResource<Material>("Materials/Mushroom_cw.xml"));
	//test_model->SetMaterial(cache->GetResource<Material>("Materials/VColUnlit.xml"));
	//test_model->SetMaterial(cache->GetResource<Material>("Materials/VColUnlit_cw.xml"));
	return true;
}

bool ClothEngine::LoadClothMeshUV(const String& path)
{
	if (path.Empty())
		return false;

	URHO3D_LOGINFO("=== LoadClothMesh() | file_name - " + path + "===");

	// LOW lvl LOADING
	{
		unsigned lines_count = 0;
		std::string line_uv;

		std::string f_name = path.CString();
		std::ifstream file_uv(f_name.c_str());
		if (file_uv.is_open())
		{
			URHO3D_LOGINFO("=> loading cloth mesh UV - " + String(f_name.c_str()));
			mv_ClothUVs_.Clear();

			while (file_uv.good())
			{
				getline(file_uv, line_uv);
				String r_uv = line_uv.c_str();

				Vector2 val_pos = ToVector2(r_uv);

				mv_ClothUVs_.Push(val_pos);
				++lines_count;
			}

			file_uv.close();
		}
	}

		return true;
}


int ClothEngine::ComputeClothUVs()
{
	if (mv_CLOTH_MESH_POS_.Empty())
		return -1;

	URHO3D_LOGINFO("=== ComputeClothUVs() | mi_ClothTexW_: " + String(mi_ClothTexW_) + " | mi_ClothTexH_: " + String(mi_ClothTexH_) + "===");
	mv_ClothUVs_.Clear();

	Camera* camera = cameraNode_->GetComponent<Camera>();
	if (camera)
	{
		Renderer* p_render = GetSubsystem<Renderer>();
		if (p_render)
		{
			Viewport* p_view = p_render->GetViewport(0);
			if (p_view)
			{
				for (int i = 0; i < mv_CLOTH_MESH_POS_.Size(); ++i)
				{
					Vector2 uv = Vector2::ZERO;
					IntVector2 i_uv = p_view->WorldToScreenPoint(mv_CLOTH_MESH_POS_[i]);

					float X = static_cast<float>(i_uv.x_) / static_cast<float>(490);
					float Y = static_cast<float>(i_uv.y_) / static_cast<float>(562);
					uv.x_ = X;
					uv.y_ = Y;

					URHO3D_LOGINFO("	V[" + String(i) + "]  W: " + mv_CLOTH_MESH_POS_[i].ToString() +
						" | S: " + i_uv.ToString() + " | UV: " + uv.ToString());

					mv_ClothUVs_.Push(uv);
				}
			}
		}
	}

	return mv_ClothUVs_.Size();
}

void ClothEngine::WriteClothUVs(const String& filename)
{
	if (mv_ClothUVs_.Empty())
		mv_ClothUVs_.Clear();

	URHO3D_LOGINFO("=== WriteClothUVs() | mv_ClothUVs_: " + String(mv_ClothUVs_.Size()));

	String sys_path = GetSubsystem<FileSystem>()->GetProgramDir();
	String file_name = "uv_0";

	std::freopen(file_name.CString(), "w", stdout);

	// save to text file
	for (unsigned i = 0; i < mv_ClothUVs_.Size(); ++i)
	{
		Vector2 w_pos_write = mv_ClothUVs_[i];
		//URHO3D_LOGINFO("=> SAVE w_pos P[" + String(i) + "]:" + w_pos.ToString());

		// write to file
		std::cout << w_pos_write.ToString().CString() << std::endl;
	}

	fclose(stdout);
}

PODVector<VertexElement> ClothEngine::GetVertexElements() const
{
	URHO3D_LOGINFO("*** GetVertexElements ***");

	PODVector<VertexElement> ret;

	// Position must always be first and of type Vector3 for raycasts to work
	ret.Push(VertexElement(TYPE_VECTOR3, SEM_POSITION));

	if (!mv_CLOTH_MESH_NORMALS_.Empty() && (mv_CLOTH_MESH_NORMALS_.Size() == mv_CLOTH_MESH_POS_.Size()))
	{
		URHO3D_LOGINFO("...mesh has normals...");
		ret.Push(VertexElement(TYPE_VECTOR3, SEM_NORMAL));
	}

	if (!mv_ClothUVs_.Empty() && (mv_ClothUVs_.Size() == mv_CLOTH_MESH_POS_.Size()))
	{
		URHO3D_LOGINFO("...mesh has UVs...");
		ret.Push(VertexElement(TYPE_VECTOR2, SEM_TEXCOORD));
	}

	return ret;
}



bool ClothEngine::ScaleClothMesh(const Vector3 world_scale)
{
	// Scale Cloth Mesh
	{
		mb_DrawClothMesh_ = false;

		for (auto& i : mv_CLOTH_MESH_POS_)
			i *= world_scale;

		/*for (auto& i : mv_ClothTextureContour_)
		{
			if (i.second_)
			{
				i.second_->Scale(world_scale);
			}
		}

		for (auto& i : mv_ClothTextureContourClone_)
			i *= world_scale;*/

		//mb_DrawClothMesh_ = true;
	}

	return true;
}

bool ClothEngine::MoveClothMesh(const Vector3 world_v)
{
	URHO3D_LOGINFO("=== MoveClothMesh() | world_v: " + world_v.ToString() + " ===");

	// node moving
	{
		//if (!mp_ClothMeshNode_)
		//	return false;
		////mp_ClothMeshNode_->Translate(world_v);
	}

	{
		mb_DrawClothMesh_ = false;

		for (auto& i : mv_CLOTH_MESH_POS_)
			i += world_v;

		//mb_DrawClothMesh_ = true;
	}

	return true;
}

unsigned ClothEngine::CheckContourMeshCompare()
{
	if (mv_ClothTextureContour_.Empty() || mv_ClothTextureContourClone_.Empty())
		return 0;

	if (mv_CLOTH_MESH_POS_.Empty() || mv_CLOTH_MESH_INDICES_.Empty())
		return 0;

	URHO3D_LOGINFO("=== CheckContourMeshCompare() | mv_ClothTextureContour_ size: " + String(mv_ClothTextureContour_.Size()) + " ===");

	// compare precalculated cloth CPs in loaded Cloth mesh
	for (int i = 0; i < mv_ClothTextureContourClone_.Size(); ++i)
	{
		for (int j = 0; j < mv_CLOTH_MESH_POS_.Size(); ++j)
		{
			if (mv_ClothTextureContourClone_[i] == mv_CLOTH_MESH_POS_[j])
			{
				/*URHO3D_LOGINFO("=> vertex[" + String(i) + "]: " + mv_ClothTextureContourClone_[i].ToString() + " FINDED in cloth mesh - " +
					String(j));*/

				Pair<int, int> pair;
				pair.first_ = i;
				pair.second_ = j;
				vpr_ContourMeshCorresponded_.Push(pair);
			}
		}
	}

	// CHECK WITH NODES CPs
	/*for (unsigned i = 0; i < mv_ClothTextureContour_.Size(); ++i)
	{
	Vector3 node_wpos = mv_ClothTextureContour_[i].second_->GetWorldPosition();
	for (unsigned j = 0; j < mv_CLOTH_MESH_POS_.Size(); ++j)
	{
	if (node_wpos == mv_CLOTH_MESH_POS_[j])
	{
	URHO3D_LOGINFO("=> vertex[" + String(i) + "]: " + node_wpos.ToString() + " FINDED in cloth mesh - " +
	String(j));

	v_ClothMeshCorresponded.Push(j);
	}
	}
	}*/

	return vpr_ContourMeshCorresponded_.Size();
}

bool ClothEngine::AdjustCMeshToTextureNode()
{
	if (mv_ClothTextureContour_.Empty() || mv_ClothTextureContourClone_.Empty())
		return false;

	if (mv_CLOTH_MESH_POS_.Empty() || mv_CLOTH_MESH_INDICES_.Empty())
		return false;

	URHO3D_LOGINFO("=== AdjustCMeshToTextureNode() | mv_ClothTextureContour_ size: " + String(mv_ClothTextureContour_.Size()) + " ===");

	Vector3 v3_tex_v0 = mv_ClothTextureContour_.At(0).second_->GetWorldPosition();
	Vector3 v3_mesh_v0 = mv_CLOTH_MESH_POS_.At(0);

	/*if (v3_tex_v0 != v3_mesh_v0)
	{
	URHO3D_LOGERROR("!!! v3_tex_v0 != v3_mesh_v0 !!!");
	return false;
	}*/

	Vector3 v3_diff = v3_tex_v0 - v3_mesh_v0;
	URHO3D_LOGERROR("=> difference between TEX & MESH - " + v3_diff.ToString());

	{
		mb_DrawClothMesh_ = false;

		for (auto& i : mv_CLOTH_MESH_POS_)
			i += v3_diff;

		// move mesh contour to texture contours
		URHO3D_LOGERROR("=> adjust contour vertices MESH -> TEX ");
		for (unsigned i = 0; i < mv_ClothTextureContour_.Size(); ++i)
		{
			Vector3 curr_mesh_wpos = mv_CLOTH_MESH_POS_[i];
			//URHO3D_LOGERROR("	proccess P["+String(i)+"]");
			Vector3 curr_tex_wpos = mv_ClothTextureContour_.At(i).second_->GetWorldPosition();
			curr_mesh_wpos.x_ = curr_tex_wpos.x_;
			curr_mesh_wpos.y_ = curr_tex_wpos.y_;

			mv_CLOTH_MESH_POS_[i] = curr_mesh_wpos;
		}
	}

	return true;
}
