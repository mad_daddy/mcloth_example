//
// Created by xxxantikvarxxx on 8/9/17.
//

#pragma once

//#include <cppcms/application.h>
//#include <cppcms/applications_pool.h>
//#include <cppcms/service.h>
//
//#include <cppcms/http_file.h>
//#include <cppcms/http_response.h>
//#include <cppcms/url_dispatcher.h>
//#include <cppcms/url_mapper.h>
//
//#include <cppcms/json.h>
//#include <booster/log.h>
//
//#include <iostream>
//#include <fstream>
//#include <stdlib.h>
//
//#include "transfer.h"
//
//using namespace std;
//
//class api_handlers : public cppcms::application
//{
//public:
//	api_handlers(cppcms::service &s) :
//		cppcms::application(s)
//	{
//
//		dispatcher().assign("/test", &api_handlers::test, this);
//		mapper().assign("test", "/test");
//	}
//
//	void test()
//	{
//		cppcms::json::value res;
//		res["test"] = "Test";
//
//		// ENGINE START FUNC
////        int Transfer(const std::string b_i, // backimage
////                     const std::string m_49, // 49 points
////                     const std::string c_i, // cloth image
////                     const std::string c_c, // cloth contour
////                     const std::string c_m, // cloth mesh
////                     const std::string out); // output path
//        Transfer(
//                "data/Test/back.png",
//                "data/Test/4test.png_frontal.ljson",
//                "data/Test/cloth.png",
//                "data/Test/contour_0.xml",
//                "data/Test/test_0.obj",
//                "data/Test/out.png"
//        );
//        response().out() << res;
//	}
//};
//
//class api : public cppcms::application
//{
//public:
//    api(cppcms::service &srv) :
//            cppcms::application(srv)
//	{
//        attach(new api_handlers(srv),
//               "handler", "/handler{1}", // mapping
//               "/handler(/(.*))?", 1);   // dispatching
//
//        dispatcher().assign("", &api::describe, this);
//        mapper().assign(""); // default URL
//
//        mapper().root("/api");
//    }
//
//    void main(string url)
//	{
//        response().set_header("Access-Control-Allow-Origin", "*");
//        response().set_header("Access-Control-Allow-Credentials", "true");
//        response().set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
//        response().set_header("Access-Control-Allow-Headers",
//                              "X-Access-Token, X-Application-Name, X-Request-Sent-Time, Content-Type");
//
////        if (cppcms::http::request().request_method() == "OPTIONS") {
////            return;
////        }
//        cppcms::application::main(url);
//    }
//
//    void describe()
//	{
//        response().out()
//                << "RESTServer API";
//    }
//};
