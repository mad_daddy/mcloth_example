
// Created by mad_daddy

#pragma once

#include "../Core/Context.h"
#include <Urho3D/Core/CoreEvents.h>

#include "../Engine/Engine.h"

#include "../IO/IOEvents.h"
#include "../IO/Log.h"
#include <Urho3D/IO/FileSystem.h>


#include <Urho3D/Resource/ResourceCache.h>

#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>

#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Cursor.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/UI/Window.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Text3D.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/UI/DropDownList.h>
#include <Urho3D/UI/Slider.h>
#include <Urho3D/UI/CheckBox.h>
#include <Urho3D/UI/FileSelector.h>

#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/RenderPath.h>

#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Technique.h>
#include <Urho3D/Graphics/Texture2D.h>

#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/AnimatedModel.h>

#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/DebugRenderer.h>


#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>

/// Urho namespace
using namespace Urho3D;


/// Cloth Engine class
class ClothEngine : public Object
{
	URHO3D_OBJECT(ClothEngine, Object);

public:
	/// Construct
	ClothEngine(Context* context);

	/// Setup before engine initialization. This is a chance to eg. modify the engine parameters. Call ErrorExit() to terminate without initializing the engine. Called by Application.
	void Setup();
	/// Setup after engine initialization and before running the main loop. Call ErrorExit() to terminate without running the main loop. Called by Application.

	void SetupViewport();

	void Start();
	/// Cleanup after the main loop. Called by Application.
	void Stop();
	/// Initialize the engine and run the main loop, then return the application exit code. Catch out-of-memory exceptions while running.
	int Run();
	/// Show an error message (last log message if empty), terminate the main loop, and set failure exit code.
	void ErrorExit(const String& message = String::EMPTY);

	/// Initialize mouse mode on non-web platform.
	void InitMouseMode(MouseMode mode);


	// ====== MENPO POINTSET ======
	struct MenpoPS : public RefCounted
	{
		/// def construct
		MenpoPS() {};

		/// Remove duplicates from ORIG Menpo PS
		unsigned RemoveMenpoPS_ORIG_Dups();
		/// Rotate origin Menpo PointSet
		void RotateMORIGMenpoPS(const float x_degrees, const float y_degrees);
		/// Adjust with projected model params
		void AdjustMORIGMenpoPS(const Vector2 menpo_scale, bool menpo_rot, bool move_to_center);

		/// skeleton joints
		bool GetMenpoJointPos();
		/// skeleton joints
		bool GetMenpoJointPos_OLD();

		/// Enabling projection
		void EnableMPSProjection(bool enabled);
		/// Enabling flag
		bool m_bMPSProjEnabled_ = true;

		/// GETTERS
		Vector2* GetMenpoPoint(const unsigned idx);
		Node* GetMenpoPointNode(const unsigned idx);

		float GetMOriginPSHeight();
		float GetMOriginPSWidth();

		float GetMOriginPS_Legs_H() const;
		float GetMOriginPS_Body_H() const;

		float GetMProjPSHeight();
		float GetMprojPSWidth();

		float GetMProjPS_Legs_H() const;
		float GetMProjPS_Body_H() const;


		/// Original Menpo PointSet pos
		Vector<Pair<unsigned, Vector2>> m_ORIG_MenpoPS_List;
		/// Projected Menpo PointSet pos
		Vector<Pair<unsigned, Vector2>> m_PROJ_MenpoPS_List;
		/// Scene visual nodes
		Vector<Pair<unsigned, Node*>> MENPO_POINT_SET;


		/// Menpo Skeleton Joints
		HashMap<String, Vector2> MENPO_JOINTS;

		/// MENPO PointSet center
		Vector2 m_OrigMPointSetCtr_;
		Pair<Vector2, Node*> m_ProjMPointSetCtr_;
		/// prev center
		Pair<Vector2, Node*> m_PrevProjMPointSetCtr_;


		/// Menpo Point Pair related to projected params
		Vector<Pair<unsigned, unsigned>> M_RELATED_POINTS;

		/// Min/Max Y coords (Orig/Proj)
		Pair<unsigned, Vector2> m_Orig_Min_Y, m_Orig_Max_Y;
		Pair<unsigned, Vector2> m_Proj_Min_Y, m_Proj_Max_Y;

		/// Min/Max X coords (Orig/Proj)
		Pair<unsigned, Vector2> m_Orig_Min_X, m_Orig_Max_X;
		Pair<unsigned, Vector2> m_Proj_Min_X, m_Proj_Max_X;

		/// Projection flag
		bool m_bMPSProjAdjusted_ = false;

		/// Original MPS Height/width
		float m_OMPS_ScreenHeight_ = 0.0f;
		float m_PMPS_WorldHeight_ = 0.0f;
		Vector3 mv3_PMPS_WP_;

		float m_OMPS_ScreenWidth_ = 0.0f;
		float m_PMPS_WorldWidth_ = 0.0f;

		/// Original MPS Legs Height
		float m_OMPS_Legs_ScreenHeight_ = 0.0f;
		float m_PMPS_Legs_WorldHeight_ = 0.0f;
		Vector3 mv3_PMPS_Legs_WP_;

		/// Original MPS Body Height
		float m_OMPS_Body_ScreenHeight_ = 0.0f;
		float m_OMPS_Body_WorldHeight_ = 0.0f;
		Vector3 mv3_PMPS_Body_WP_;
	};

	/// Menpo PointSet
	SharedPtr<MenpoPS> mp_MPS_OLD_;

	// contour drawing flag
	bool mb_drawClothContour_ = false;
	bool mb_ResetClothContour_ = true;

	/// Camera distance to model
	float mf_Z_Dist_ = 0.0f;

	/// Load from JSON 
	unsigned LoadMENPOPointSetJSON(const String& path, bool remove_duplicates, SharedPtr<MenpoPS>& target);
	/// Get Orig MPS Screen Measures
	bool GetOrigMPScreenMeasures(SharedPtr<MenpoPS>& target);

	// ====== DEMO METHODS ======
	/// Init default Back Image
	void InitDefaultMENPOBackImage();
	/// Load Back Image
	bool LoadBackImage(const String& path, bool apply_scale);
	/// Calculate BackImage scale
	Pair<float, float> CalcBackImgScale(const int t_width, const int t_height);
	
	// === M_49 ====
	/// Project Origin Menpo PointSet onto model projection
	bool ProjectMORIGMenpoPS(const Vector2 p_scale, float depth, SharedPtr<MenpoPS>& target, bool create_nodes, bool save_f);
	/// Get Orig MPS World Measures
	int GetProjMPWorldMeasures(SharedPtr<MenpoPS>& target);
	/// Draw MENPO PointSet as a contour
	void DrawMenpoPROJ_PS(SharedPtr<MenpoPS>& target) const;

	/// --- Back Image ---
	SharedPtr<Texture2D> mp_BackImage;
	float mf_BackImage_scale_ = 0.0f;
	float mf_BackImage_scale_h = 0.0f;
	float mf_BackImage_scale_w = 0.0f;

	// back image height scale factor to screen mode
	float mf_BackImgHeightScale_ = 0.0f;

	// initial back image w/h
	int mu_BackImgInitHeight = 0;
	int mu_BackImgInitWidth = 0;
	// updated back image w/h
	int mi_BackImgHeight = 0;
	int mi_BackImgWidth = 0;


	// ====== CLOTH Texture ======

	// Load CLOTH Texture
	bool LoadClothTexture(const String& path);
	// Texture
	SharedPtr<Texture2D> mp_ClothTex_;
	float mf_ClothTex_scale_ = 0.0f;
	float mf_ClothTex_scale_h = 0.0f;
	float mf_ClothTex_scale_w = 0.0f;

	int mi_ClothTex_height = 0;
	int mi_ClothTex_width = 0;

	// ORIGINAL
	int mi_ClothTexH_ = 0;
	int mi_ClothTexW_ = 0;

	// Cloth Measures from menpo-49 CPM
	unsigned GetClothMeasures(SharedPtr<MenpoPS>& target);
	Vector<Pair<unsigned, Vector3*>> mv_ClothSelectedContour_;
	Pair<Vector3, Vector3> mpr_ClothNeck_;
	Vector3 v3_LErpedNeck_;
	Pair<float, float> mf_NeckDist_;

	Pair<Vector3, Vector3> mpr_ClothShoulders_;
	Vector3 v3_LErpedShoulders_;
	Pair<float, float> mf_ShouldersDist_;

	Pair<Vector3, Vector3> mpr_ClothWaist_;
	Vector3 v3_LErpedWaist_;
	Pair<float, float> mf_WaistDist_;

	Pair<Vector3, Vector3> mpr_ClothCroch_;
	Vector3 v3_LErpedCroch_;
	Pair<float, float> mf_CrochDist_;

	Pair<Vector3, Vector3> mpr_ClothLHand_;
	Vector3 v3_LErpedLHand_;
	Pair<float, float> mf_LHandDist_;

	Pair<Vector3, Vector3> mpr_ClothRHand_;
	Vector3 v3_LErpedRHand_;
	Pair<float, float> mf_RHandDist_;

	// MPS-49 neck-croch height
	float mf_NeckToCrochHeight_ = 0.0f;
	Vector3 v3_NeckToCrochCenter_;

	// MEASURED dists
	float mf_ClothHeight_ = 0.0f;
	float mf_ClothWidth_ = 0.0f;

	// ====== COMPUTED W/H
	float mf_ClothComputedWScaleX_ = 0.0f;
	float mf_ClothComputedHScaleY_ = 0.0f;

	// Cloth Plane node
	Node* m_Cloth_Plane_ = nullptr;
	Node* mp_ClothCenterNode_ = nullptr;

	Vector3 mv3_ClothPlaneInitPos_;
	Vector3 mv3_ClothPlaneInitScale_;

	// scale factor
	float mf_ClothPlaneSceleStep_ = 0.25f;

	// show cloth tex
	bool mb_ShowClothPlane_ = true;

	// scale cloth texture
	bool ScaleClothTexture(Vector3 world_scale);
	// move cloth center
	bool MoveClothTexture(/*Vector2 screen_v, */Vector3 world_v);


	// Load Contour
	unsigned LoadClothTextuteContour(const String& file_name, bool attached, bool draw);
	void VizClothTextuteContour(bool attached);

	// Pick Contour
	bool PickClothTextuteContour(const IntVector2 target_pixel);
	bool SaveClothTextuteContour(const String& path);

	/// Cloth Contour geom center
	Pair<Vector3, Node*> mpr_ClothContourCenter_;

	/// Picked Contour
	Vector<Pair<IntVector2, Node*>> mv_ClothTextureContour_;
	/// CLOTH IMAGE CONTOUR
	Vector<Vector3> mv_ClothTextureContourClone_;

	// Recompute CONTOUR MEASURES
	bool RecomputeContourMeasures(bool scene_nodes);

	/// debug mv_ClothTextureContour_
	void DrawClothTexContour();

	Vector3 v3_ContourShouldersL;
	Vector3 v3_ContourShouldersR;
	Vector3 v3_ContourshouldersCenter;

	Vector3 v3_ContourCrochL;
	Vector3 v3_ContourCrochR;
	Vector3 v3_ContourCrochCenter;

	// Load cloth Mesh
	bool LoadClothMesh(const String& file_name, const bool draw);
	// form cloth mesh
	bool FormClothMesh(const String& file_name);
	// Load formed clotth mesh
	bool LoadFormedClothMesh(const String& path);

	// Cloth_Model node
	Node* mp_Cloth_Node_ = nullptr;

	// load cloth mesh uv
	bool LoadClothMeshUV(const String& path);
	// Compute UV
	int ComputeClothUVs();
	// write UV
	void WriteClothUVs(const String& filename);

	// Mesh data
	Vector<Vector3> mv_CLOTH_MESH_POS_;
	Vector<Vector3> mv_CLOTH_MESH_NORMALS_;
	Vector<unsigned> mv_CLOTH_MESH_INDICES_;
	Vector<Vector2> mv_ClothUVs_;

	PODVector<VertexElement> GetVertexElements() const;

	// clothing mesh draw flag
	bool mb_DrawClothMesh_ = false;

	// scale cloth mesh
	bool ScaleClothMesh(const Vector3 world_scale);
	// move cloth mesh
	bool MoveClothMesh(const Vector3 world_v);

	unsigned CheckContourMeshCompare();

	Vector<Pair<int, int>> vpr_ContourMeshCorresponded_;

	// adjust corresponded CPs (idx based)
	bool AdjustCMeshToTextureNode();

	/// Mouse mode option to use in the sample.
	MouseMode useMouseMode_;
	/// Scene.
	SharedPtr<Scene> scene_;
	/// Camera scene node.
	SharedPtr<Node> cameraNode_;

	/// Camera yaw angle.
	float yaw_;
	/// Camera pitch angle.
	float pitch_;

	// ------ Scene data ------
	/// find selected model point
	int CheckSelectedNode();
	int mi_NodeSelectionMode_ = -1;

	Node* m_LastSlectedElement = nullptr;
	int m_SelectedModelPnt = -1;

	int LEFT_START_CLICK = 0;

private:
	/// Set custom window Title & Icon
	void SetWindowTitleAndIcon();

	/// Handle log message.
	void HandleLogMessage(StringHash eventType, VariantMap& eventData);
	/// Handle request for mouse mode on web platform.
	void HandleMouseModeRequest(StringHash eventType, VariantMap& eventData);
	/// Handle request for mouse mode change on web platform.
	void HandleMouseModeChange(StringHash eventType, VariantMap& eventData);
	/// Handle key down event to process key controls common to all samples.
	void HandleKeyDown(StringHash eventType, VariantMap& eventData);
	/// Handle key up event to process key controls common to all samples.
	void HandleKeyUp(StringHash eventType, VariantMap& eventData);
	/// Handle scene update event to control camera's pitch and yaw for all samples.
	void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);

	/// Handle mouse clicks
	void HandleControlClicked(StringHash eventType, VariantMap& eventData);

	/// App Handlers
	void HandleUpdate(StringHash eventType, VariantMap& eventData);
	void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

	/// Construct the scene content.
	void CreateScene();
	void CreateUI();

	/// Raycast function
	void ViewRaycast(bool left_Click, bool middle_click);
	/// Camera moving
	void MoveCamera(float timeStep);


	/// --- Debug rendering flags ---
	/// Last selected measure or param 
	WeakPtr<Drawable> mpw_LastSelectedDrawable_;
	/// Fill mode
	unsigned mb_FillMode_ = 0;
	/// General debug flag
	bool mb_DrawDebug_ = false;

	/// Urho3D engine.
	SharedPtr<Engine> engine_;
	/// Engine parameters map.
	VariantMap engineParameters_;
	/// Collected startup error log messages.
	String startupErrors_;
	/// Application exit code.
	int exitCode_;

	/// Logging flag
	bool _LOGGING_ = true;

	bool _ImageReady_ = false;

	// Back image path 
	String m_Back_Img_Path_;
	String m_M_49_Path_;

	String m_Cloth_Img_Path_;
	String m_Cloth_Contour_Path_;
	String m_Cloth_Mesh_Path_;

	// out texture path
	String m_Out_Path_;

    // side effect
    int render_finished = 0;
};


ClothEngine::ClothEngine(Context* context) :
	Object(context),
	useMouseMode_(MM_ABSOLUTE),
	yaw_(0.0f),
	pitch_(0.0f),
	exitCode_(EXIT_SUCCESS)
{
	// Create the Engine, but do not initialize it yet. Subsystems except Graphics & Renderer are registered at this point
	engine_ = new Engine(context);

	// Subscribe to log messages so that can show errors if ErrorExit() is called with empty message
	//SubscribeToEvent(E_LOGMESSAGE, URHO3D_HANDLER(ClothEngine, HandleLogMessage));	
}


void ClothEngine::Setup()
{
	// Modify engine startup parameters
	engineParameters_["WindowTitle"] = GetTypeName();
	engineParameters_["LogName"] = "Cloth.log";

	engineParameters_["FullScreen"] = false;
	engineParameters_["VSync"] = false;
	engineParameters_["TripleBuffer"] = false;
	engineParameters_["WindowWidth"] = 400;
	engineParameters_["WindowHeight"] = 300;

	engineParameters_["RenderPath"] = "CoreData/RenderPaths/ForwardCustom.xml";

	engineParameters_["Headless"] = false;
	engineParameters_["Sound"] = false;

	// Construct a search path to find the resource prefix with two entries:
	// The first entry is an empty path which will be substituted with program/bin directory -- this entry is for binary when it is still in build tree
	// The second and third entries are possible relative paths from the installed program/bin directory to the asset directory -- these entries are for binary when it is in the Urho3D SDK installation location
	if (!engineParameters_.Contains("ResourcePrefixPaths"))
		engineParameters_["ResourcePrefixPaths"] = ";../share/Resources;../share/Urho3D/Resources";
}

void ClothEngine::SetupViewport()
{
	URHO3D_LOGINFO("=== SetupViewport() ===");
	// Create the camera 
	cameraNode_ = new Node(context_);

	Vector3 new_cam_pos = Vector3::ZERO;
	new_cam_pos.x_ = 0.0f;
	new_cam_pos.y_ = 0.0f;
	new_cam_pos.z_ = -30.0f;

	mf_Z_Dist_ = fabsf(new_cam_pos.z_);

	Camera* camera = cameraNode_->CreateComponent<Camera>();
	camera->SetFarClip(300.0f);

	cameraNode_->SetPosition(new_cam_pos);
	camera->SetOrthographic(true);

	//cameraNode_->SetDirection(Vector3(1.0f, -1.0f, 1.0f));
	//cameraNode_->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
	//cameraNode_->LookAt(modelObject->GetBoundingBox().Center());

	// Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
	SharedPtr<Viewport> viewport(new Viewport(context_, scene_, camera));
	Renderer* renderer = GetSubsystem<Renderer>();
	renderer->SetViewport(0, viewport);

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	RenderPath* pRenderPath = viewport->GetRenderPath();

	// Add post-processing effects appropriate with the example scene
	renderer->SetHDRRendering(true);
	SharedPtr<RenderPath> effectRenderPath = viewport->GetRenderPath()->Clone();
	//effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/FXAA2.xml"));
	effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
   // effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/GammaCorrection.xml"));
	viewport->SetRenderPath(effectRenderPath);
}

void ClothEngine::Start()
{
	URHO3D_LOGINFO("=> cloth engine start..");
	CreateScene();

	{
       // File loadFile(context_, "Data/Scenes/HumanBodyScenes/HumanBody_175_male_standart/HumanBody_175_male_standart.xml", FILE_READ);
		File loadFile(context_, "HumanBody_175_male_standart.xml", FILE_READ);
		if(scene_->LoadXML(loadFile))
            URHO3D_LOGINFO("--- Scene loaded success ---");
	}

	// Set custom window Title & Icon
	SetWindowTitleAndIcon();

	// Subscribe key down event
	SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(ClothEngine, HandleKeyDown));
	// Subscribe key up event
	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(ClothEngine, HandleKeyUp));
	// Subscribe scene update event
	SubscribeToEvent(E_SCENEUPDATE, URHO3D_HANDLER(ClothEngine, HandleSceneUpdate));

	// Subscribe HandleControlClicked() for raycasting
	SubscribeToEvent(E_UIMOUSECLICK, URHO3D_HANDLER(ClothEngine, HandleControlClicked));
	// Subscribe HandleUpdate() function for camera motion & some scene logic update
	SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ClothEngine, HandleUpdate));
	// Subscribe HandlePostRenderUpdate() function for processing the post-render update event, sent after Renderer subsystem is
	// done with defining the draw calls for the viewports (but before actually executing them.) We will request debug geometry
	// rendering during that event
	SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(ClothEngine, HandlePostRenderUpdate));

	SetupViewport();

#if defined(_WIN32)
	CreateUI();
#endif

	if (!mp_BackImage)
		InitDefaultMENPOBackImage();

	LoadBackImage(m_Back_Img_Path_, false);

	// Initialize Menpo PS(49) - mp_MPS_OLD_ if not exist
	{
		if (mp_MPS_OLD_)
		{
			mp_MPS_OLD_.Reset();
			mp_MPS_OLD_ = nullptr;
		}

		// init MPS OLD
		mp_MPS_OLD_ = new MenpoPS();

		if (!mp_MPS_OLD_)
		{
			URHO3D_LOGERROR("-> FAILED TO CREATE MPS OLD !!!");
			return;
		}
	}

	if (LoadMENPOPointSetJSON(m_M_49_Path_, false, mp_MPS_OLD_))
	{
		URHO3D_LOGINFO("-> m_49 loaded success...");

		if (ProjectMORIGMenpoPS(Vector2(0.15f, 0.15f), mf_Z_Dist_, mp_MPS_OLD_, true, false))
			GetClothMeasures(mp_MPS_OLD_);
	}

	//if (LoadClothTexture(m_Cloth_Img_Path_))
	{
		if (LoadClothTextuteContour(m_Cloth_Contour_Path_, false, true))
		{
			URHO3D_LOGINFO("-> mv_ClothTextureContour_ size - " + String(mv_ClothTextureContour_.Size()));
			URHO3D_LOGINFO("-> mv_ClothTextureContourClone_ size - " + String(mv_ClothTextureContourClone_.Size()));

			//SaveClothTextuteContour("contour_1");
		}

		if (LoadClothMesh(m_Cloth_Mesh_Path_, true))
		{
			//ComputeClothUVs();
			//WriteClothUVs("uv_2");

			CheckContourMeshCompare();

			Vector3 w_mesh_scale = Vector3(mf_ClothComputedWScaleX_, mf_ClothComputedHScaleY_, 1.0f);
			URHO3D_LOGINFO("-> w_mesh_scale - " + w_mesh_scale.ToString());
			ScaleClothMesh(w_mesh_scale);

			// recompute contour 
			RecomputeContourMeasures(true);

			//Vector3 w_tex_scale = Vector3(mf_ClothComputedWScaleX_, 1.0f, mf_ClothComputedHScaleY_);
			//URHO3D_LOGINFO("-> w_tex_scale - " + w_tex_scale.ToString());
			//ScaleClothTexture(w_tex_scale);


			// move to shoulders diff
			// Vector3 v3_shoulders_diff = v3_LErpedShoulders_ - mpr_ClothContourCenter_.first_;
			Vector3 v3_shoulders_diff = v3_LErpedShoulders_ - v3_ContourshouldersCenter;


			//URHO3D_LOGINFO("-> v3_shoulders_diff - " + v3_shoulders_diff.ToString());
			MoveClothMesh(v3_shoulders_diff);

			// recompute contour 
			RecomputeContourMeasures(true);

			LoadClothMeshUV("uv_2");

			if(FormClothMesh("cloth.mdl"))
				URHO3D_LOGINFO("-> model saved");

			LoadFormedClothMesh("cloth.mdl");


			// vis if need
			VizClothTextuteContour(false);

			
			//Vector3 v3_tex_move = Vector3(v3_shoulders_diff.x_, 0.0f, v3_shoulders_diff.y_);
			//URHO3D_LOGINFO("-> v3_tex_move - " + v3_tex_move.ToString());
			//MoveClothTexture(v3_tex_move);

			//AdjustCMeshToTextureNode();
		}
	}


	{
		//	if (LoadClothTexture(m_Cloth_Img_Path_))
		//	{
		//		if (LoadClothTextuteContour(m_Cloth_Contour_Path_, true))
		//		{
		//			URHO3D_LOGINFO("-> mv_ClothTextureContour_ size - " + String(mv_ClothTextureContour_.Size()));
		//			URHO3D_LOGINFO("-> mv_ClothTextureContourClone_ size - " + String(mv_ClothTextureContourClone_.Size()));

		//			//SaveClothTextuteContour("contour_1");

		//			if (LoadClothMesh(m_Cloth_Mesh_Path_))
		//			{
		//				ComputeClothUVs();
		//				WriteClothUVs("uv_0");

		//				//Vector3 w_tex_scale = Vector3(mf_ClothComputedWScaleX_, 1.0f, mf_ClothComputedHScaleY_);
		//				//URHO3D_LOGINFO("-> w_tex_scale - " + w_tex_scale.ToString());
		//				//ScaleClothTexture(w_tex_scale);

		//				//Vector3 w_mesh_scale = Vector3(mf_ClothComputedWScaleX_, mf_ClothComputedHScaleY_, 1.0f);
		//				//URHO3D_LOGINFO("-> w_mesh_scale - " + w_mesh_scale.ToString());
		//				//ScaleClothMesh(w_mesh_scale);

		//				//// recompute contour 
		//				//RecomputeContourMeasures(true);

		//				//// move to shoulders diff
		//				//Vector3 v3_shoulders_diff = v3_LErpedShoulders_ - mv3_ContourshouldersCenter_;
		//				//URHO3D_LOGINFO("-> v3_shoulders_diff - " + v3_shoulders_diff.ToString());

		//				//Vector3 v3_tex_move = Vector3(v3_shoulders_diff.x_, 0.0f, v3_shoulders_diff.y_);
		//				//URHO3D_LOGINFO("-> v3_tex_move - " + v3_tex_move.ToString());
		//				//MoveClothTexture(v3_tex_move);

		//				//MoveClothMesh(v3_shoulders_diff);

		//				//AdjustCMeshToTextureNode();
		//			}
		//		}
		//	}
		//}
	}	
}

void ClothEngine::Stop()
{
	engine_->DumpResources(true);
}

int ClothEngine::Run()
{
	try
	{
        // Back image path
        m_Back_Img_Path_ = String(g_Back_Img_Path_.c_str());
        m_M_49_Path_ = String(g_M_49_Path_.c_str());

        m_Cloth_Img_Path_ = String(g_Cloth_Img_Path_.c_str());
        m_Cloth_Contour_Path_ = String(g_Cloth_Contour_Path_.c_str());
        m_Cloth_Mesh_Path_ = String(g_Cloth_Mesh_Path_.c_str());

        // out texture path
        m_Out_Path_ = String(g_Out_Path_.c_str());

		Setup();
		if (exitCode_)
			return exitCode_;

		if (!engine_->Initialize(engineParameters_))
		{
			ErrorExit();
			return exitCode_;
		}
		else
		{
			URHO3D_LOGINFO("=> engine_ initialized success..");

#if defined(_WIN32)
			m_Back_Img_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/1.jpg";
			//m_Back_Img_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/man_shirt_1.png";


			m_M_49_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/1.ljson";

			m_Cloth_Img_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/man_shirt_1.png";

			//m_Cloth_Contour_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/man_shirt_1_c.xml";
			m_Cloth_Contour_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/man_shirt_2_c.xml";
			

			//m_Cloth_Mesh_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/man_shirt_1_m.obj";
			m_Cloth_Mesh_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/man_shirt_2_m.obj";

			m_Out_Path_ = GetSubsystem<FileSystem>()->GetProgramDir() + "Data/MCloth_Data/out.png";
#endif		
		}
			

		Start();
		if (exitCode_)
			return exitCode_;

	
		while (!engine_->IsExiting() || !_ImageReady_)
		//while (!_ImageReady_)
		{
			//URHO3D_LOGINFO("=> run engine frame..");
			engine_->RunFrame();
			//URHO3D_LOGINFO("=> end engine frame..");
		}
			
			
		URHO3D_LOGINFO("=> stop engine..");
		Stop();

		return exitCode_;
	}
	catch (std::bad_alloc&)
	{
		URHO3D_LOGERROR("!!! bad_alloc catched !!!");
		//ErrorDialog(GetTypeName(), "An out-of-memory error occurred. The application will now exit.");
		return EXIT_FAILURE;
	}
}

void ClothEngine::ErrorExit(const String& message)
{
	engine_->Exit(); // Close the rendering window
	exitCode_ = EXIT_FAILURE;

	/*if (!message.Length())
	{
		ErrorDialog(GetTypeName(), startupErrors_.Length() ? startupErrors_ :
			"Application has been terminated due to unexpected error.");
	}
	else
		ErrorDialog(GetTypeName(), message);*/
}

void ClothEngine::InitMouseMode(MouseMode mode)
{
	useMouseMode_ = mode;

	Input* input = GetSubsystem<Input>();

	//if (GetPlatform() != "Web")
	{
		if (useMouseMode_ == MM_FREE)
			input->SetMouseVisible(true);

		//Console* console = GetSubsystem<Console>();
		if (useMouseMode_ != MM_ABSOLUTE)
		{
			input->SetMouseMode(useMouseMode_);
			//if (console && console->IsVisible())
			//	input->SetMouseMode(MM_ABSOLUTE, true);
		}
	}
	/*else
	{
		input->SetMouseVisible(true);
		SubscribeToEvent(E_MOUSEBUTTONDOWN, URHO3D_HANDLER(ClothEngine, HandleMouseModeRequest));
		SubscribeToEvent(E_MOUSEMODECHANGED, URHO3D_HANDLER(ClothEngine, HandleMouseModeChange));
	}*/
}

void ClothEngine::SetWindowTitleAndIcon()
{
	URHO3D_LOGINFO("=== SetWindowTitleAndIcon ===");

	//Graphics* graphics = GetSubsystem<Graphics>();

	/*ResourceCache* cache = GetSubsystem<ResourceCache>();
	Image* icon = cache->GetResource<Image>("Textures/UrhoIcon.png");
	graphics->SetWindowIcon(icon);*/
	//graphics->SetWindowTitle("Clothing Engine");
}


// ---  BAse Handlers ---
void ClothEngine::HandleLogMessage(StringHash eventType, VariantMap& eventData)
{
	//using namespace LogMessage;

	//if (eventData[P_LEVEL].GetInt() == LOG_ERROR)
	//{
	//	// Strip the timestamp if necessary
	//	String error = eventData[P_MESSAGE].GetString();
	//	unsigned bracketPos = error.Find(']');
	//	if (bracketPos != String::NPOS)
	//		error = error.Substring(bracketPos + 2);

	//	startupErrors_ += error + "\n";
	//}
}

void ClothEngine::HandleMouseModeRequest(StringHash /*eventType*/, VariantMap& eventData)
{
	/*Console* console = GetSubsystem<Console>();
	if (console && console->IsVisible())
		return;*/
	Input* input = GetSubsystem<Input>();
	if (useMouseMode_ == MM_ABSOLUTE)
		input->SetMouseVisible(false);
	else if (useMouseMode_ == MM_FREE)
		input->SetMouseVisible(true);
	input->SetMouseMode(useMouseMode_);
}

void ClothEngine::HandleMouseModeChange(StringHash /*eventType*/, VariantMap& eventData)
{
	Input* input = GetSubsystem<Input>();
	bool mouseLocked = eventData[MouseModeChanged::P_MOUSELOCKED].GetBool();
	input->SetMouseVisible(!mouseLocked);
}

void ClothEngine::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
	/*if (_LOGGING_)
		URHO3D_LOGINFO("-> start HandleKeyDown..");*/

	using namespace KeyDown;

	int key = eventData[P_KEY].GetInt();

	if (key == '1')
	{
		SaveClothTextuteContour("man_contour_2");
	}
	

	// Common rendering quality controls, only when UI has no focused element
	{
		//if (!GetSubsystem<UI>()->GetFocusElement())
		//{
		//	Renderer* renderer = GetSubsystem<Renderer>();

		//	// Texture quality
		//	if (key == '1')
		//	{
		//		int quality = renderer->GetTextureQuality();
		//		++quality;
		//		if (quality > QUALITY_HIGH)
		//			quality = QUALITY_LOW;
		//		renderer->SetTextureQuality(quality);
		//	}

		//	// Material quality
		//	if (key == '2')
		//	{
		//		int quality = renderer->GetMaterialQuality();
		//		++quality;
		//		if (quality > QUALITY_HIGH)
		//			quality = QUALITY_LOW;
		//		renderer->SetMaterialQuality(quality);
		//	}

		//	// Specular lighting
		//	if (key == '3')
		//		renderer->SetSpecularLighting(!renderer->GetSpecularLighting());

		//	// Shadow rendering
		//	if (key == '4')
		//		renderer->SetDrawShadows(!renderer->GetDrawShadows());

		//	// Shadow map resolution
		//	if (key == '5')
		//	{
		//		int shadowMapSize = renderer->GetShadowMapSize();
		//		shadowMapSize *= 2;
		//		if (shadowMapSize > 2048)
		//			shadowMapSize = 512;
		//		renderer->SetShadowMapSize(shadowMapSize);
		//	}

		//	// Shadow depth and filtering quality
		//	if (key == '6')
		//	{
		//		ShadowQuality quality = renderer->GetShadowQuality();
		//		quality = (ShadowQuality)(quality + 1);
		//		if (quality > SHADOWQUALITY_BLUR_VSM)
		//			quality = SHADOWQUALITY_SIMPLE_16BIT;
		//		renderer->SetShadowQuality(quality);
		//	}

		//	// Occlusion culling
		//	if (key == '7')
		//	{
		//		bool occlusion = renderer->GetMaxOccluderTriangles() > 0;
		//		occlusion = !occlusion;
		//		renderer->SetMaxOccluderTriangles(occlusion ? 5000 : 0);
		//	}

		//	// Instancing
		//	if (key == '8')
		//		renderer->SetDynamicInstancing(!renderer->GetDynamicInstancing());

		//	// Take screenshot
		//	if (key == '9')
		//	{
		//		Graphics* graphics = GetSubsystem<Graphics>();
		//		Image screenshot(context_);
		//		graphics->TakeScreenShot(screenshot);

		//		// Here we save in the Data folder with date and time appended
		//		screenshot.SavePNG(GetSubsystem<FileSystem>()->GetProgramDir() + "Data/Screenshot_" +
		//			Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_') + ".png");
		//	}
		//}
	}

	//if (render_finished == 1)
	//{
	//	// save to png
	//	{
	//		Graphics* graphics = GetSubsystem<Graphics>();
	//		Image screenshot(context_);
	//		graphics->TakeScreenShot(screenshot);

	//		// Here we save in the Data folder with date and time appended
	//		if (screenshot.SavePNG(m_Out_Path_))
	//		{
	//			_ImageReady_ = true;
	//			URHO3D_LOGINFO("_ImageReady_ - " + String(_ImageReady_) + "");
	//			//engine_->Exit();
	//			//exitCode_ = 0;
	//		}
	//	}
	//}

	/*if (_LOGGING_)
		URHO3D_LOGINFO("-> end HandleKeyDown..");*/
}

void ClothEngine::HandleKeyUp(StringHash /*eventType*/, VariantMap& eventData)
{
	using namespace KeyUp;

	int key = eventData[P_KEY].GetInt();

	// Close console (if open) or exit when ESC is pressed
	if (key == KEY_ESCAPE)
		engine_->Exit();
}

void ClothEngine::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
	//// Move the camera by touch, if the camera node is initialized by descendant sample class
	//if (touchEnabled_ && cameraNode_)
	//{
	//	Input* input = GetSubsystem<Input>();
	//	for (unsigned i = 0; i < input->GetNumTouches(); ++i)
	//	{
	//		TouchState* state = input->GetTouch(i);
	//		if (!state->touchedElement_)    // Touch on empty space
	//		{
	//			if (state->delta_.x_ || state->delta_.y_)
	//			{
	//				Camera* camera = cameraNode_->GetComponent<Camera>();
	//				if (!camera)
	//					return;

	//				Graphics* graphics = GetSubsystem<Graphics>();
	//				yaw_ += TOUCH_SENSITIVITY * camera->GetFov() / graphics->GetHeight() * state->delta_.x_;
	//				pitch_ += TOUCH_SENSITIVITY * camera->GetFov() / graphics->GetHeight() * state->delta_.y_;

	//				// Construct new orientation for the camera scene node from yaw and pitch; roll is fixed to zero
	//				cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
	//			}
	//			else
	//			{
	//				// Move the cursor to the touch position
	//				Cursor* cursor = GetSubsystem<UI>()->GetCursor();
	//				if (cursor && cursor->IsVisible())
	//					cursor->SetPosition(state->position_);
	//			}
	//		}
	//	}
	//}
}


// --- Cloth Demo Handlers ---
void ClothEngine::HandleControlClicked(StringHash eventType, VariantMap& eventData)
{
	URHO3D_LOGINFO("--- HandleControlClicked ---");

	Input* input = GetSubsystem<Input>();

	if (input->GetMouseButtonDown(MOUSEB_LEFT))
		ViewRaycast(true, false);

	// middle click
	{
		if (input->GetMouseButtonDown(MOUSEB_MIDDLE))
		{
			// Get control that was clicked
			UIElement* clicked = static_cast<UIElement*>(eventData[UIMouseClick::P_ELEMENT].GetPtr());

			ViewRaycast(false, true);
		}
	}
}

void ClothEngine::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
	if(_LOGGING_)
		URHO3D_LOGINFO("-> start update..");

#if !defined(_WIN32)
	if (render_finished == 1)
	{
		// save to png
		{
			Graphics* graphics = GetSubsystem<Graphics>();
			Image screenshot(context_);
			if(graphics->TakeScreenShot(screenshot))
				URHO3D_LOGINFO("-> backbuffer dumped success..");

			// Here we save in the Data folder with date and time appended
			if (screenshot.SavePNG(m_Out_Path_))
			{
				_ImageReady_ = true;
				URHO3D_LOGINFO("_ImageReady_ - " + String(_ImageReady_) + "");
				engine_->Exit();
				exitCode_ = EXIT_SUCCESS;
			}
		}
	}
#endif


	// other updates
	{
		//using namespace Update;

		// Take the frame time step, which is stored as a float
		//float timeStep = eventData[P_TIMESTEP].GetFloat();

		// Move the camera, scale movement with time step
		//MoveCamera(timeStep);

		// update cursor pos
		//	{
		//		UI* ui = GetSubsystem<UI>();
		//		IntVector2 pos = ui->GetCursorPosition();
		//
		//		// Check the cursor is visible and there is no UI element in front of the cursor
		//		if (ui->GetCursor()->IsVisible() || !ui->GetElementAt(pos, true))
		//		{
		//			/*if (mp_Cursor_Pos_txt_)
		//				mp_Cursor_Pos_txt_->SetText("cursor: " + pos.ToString());*/
		//		}
		//	}

		/*if (GetSubsystem<Input>()->GetKeyPress(KEY_F))
		{
		switch (mb_FillMode_)
		{
		case(0u) :
		{
		Renderer* renderer = GetSubsystem<Renderer>();
		renderer->GetViewport(0u)->GetCamera()->SetFillMode(FillMode::FILL_WIREFRAME);
		break;
		}
		case(1u) :
		{
		Renderer* renderer = GetSubsystem<Renderer>();
		renderer->GetViewport(0u)->GetCamera()->SetFillMode(FillMode::FILL_POINT);
		break;
		}
		case(2u) :
		{
		Renderer* renderer = GetSubsystem<Renderer>();
		renderer->GetViewport(0u)->GetCamera()->SetFillMode(FillMode::FILL_SOLID);
		break;
		}
		default:
		break;
		}

		mb_FillMode_ >= 2u ? mb_FillMode_ = 0u : ++mb_FillMode_;
		}

		/*if (GetSubsystem<Input>()->GetKeyPress(KEY_O))
		{
		Camera* camera = cameraNode_->GetComponent<Camera>();
		if (camera->IsOrthographic())
		{
		camera->SetOrthographic(false);
		URHO3D_LOGINFO("IsOrthographic - " + String(camera->IsOrthographic()));
		}
		else
		{
		camera->SetOrthographic(true);
		URHO3D_LOGINFO("IsOrthographic - " + String(camera->IsOrthographic()));
		}
		}*/

		//	if (GetSubsystem<Input>()->GetKeyPress(KEY_M))
		//	{
		//		if (mp_MPS_OLD_)
		//		{
		//			mp_MPS_OLD_->m_bMPSProjEnabled_ = mp_MPS_OLD_->m_bMPSProjEnabled_ ? mp_MPS_OLD_->m_bMPSProjEnabled_ = false : mp_MPS_OLD_->m_bMPSProjEnabled_ = true;
		//
		//			URHO3D_LOGINFO(" SHOW OLD MENPO PS ");
		//			mp_MPS_OLD_->EnableMPSProjection(mp_MPS_OLD_->m_bMPSProjEnabled_);
		//		}
		//	}
	}
	

	if (_LOGGING_)
		URHO3D_LOGINFO("-> end update..");
}

void ClothEngine::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
	/*if (_LOGGING_)
		URHO3D_LOGINFO("-> start post render update..");*/

	// other post render debug
	{
		/*if (mb_DrawDebug_)
		GetSubsystem<Renderer>()->DrawDebugGeometry(false);*/

		//if (mpw_LastSelectedDrawable_)
		//	mpw_LastSelectedDrawable_->DrawDebugGeometry(scene_->GetComponent<DebugRenderer>(), false);
	}
	
	// Visualize Projected OLD MENPO PS & contour
	if (mp_MPS_OLD_ && mp_MPS_OLD_->m_bMPSProjEnabled_)
		DrawMenpoPROJ_PS(mp_MPS_OLD_);

	// Visualize Cloth Contour
	DrawClothTexContour();

#if defined(_WIN32)
	//if (!_ImageReady_)
	//{
	//	// save to png
	//	{
	//		Graphics* graphics = GetSubsystem<Graphics>();
	//		Image screenshot(context_);
	//		if (graphics->TakeScreenShot(screenshot))
	//			URHO3D_LOGINFO("-> backbuffer dumped success..");

	//		// Here we save in the Data folder with date and time appended
	//		if (screenshot.SavePNG(m_Out_Path_))
	//		{
	//			_ImageReady_ = true;
	//			URHO3D_LOGINFO("_ImageReady_ - " + String(_ImageReady_) + "");
	//			engine_->Exit();
	//			exitCode_ = EXIT_SUCCESS;
	//		}
	//	}
	//}
#endif

	//render_finished = 1;

	if (_LOGGING_)
		URHO3D_LOGINFO("-> end post render update..");
}



//// ------------- Write data export ----------------
//void WriteIndices(unsigned short*& dest, SharedPtr<Mesh> pMesh, unsigned index, unsigned offset)
//{
//	if (pMesh->mFaces[index].fNumIndices == 3)
//	{
//		*dest++ = pMesh->mFaces[index].fIndices[0]; +offset;
//		*dest++ = pMesh->mFaces[index].fIndices[1]; +offset;
//		*dest++ = pMesh->mFaces[index].fIndices[2]; +offset;
//	}
//}

//void WriteVertex(float*& dest, SharedPtr<Mesh> pMesh, unsigned index, bool isSkinned, BoundingBox& box,
//	const Matrix3x4& vertexTransform, const Matrix3& normalTransform)
//{
//	Vector3 vertex = vertexTransform * pMesh->mVertices[index];
//	box.Merge(vertex);
//	*dest++ = vertex.x_;
//	*dest++ = vertex.y_;
//	*dest++ = vertex.z_;
//
//	if (pMesh->mNormals.Size() > 0u)
//	{
//		Vector3 normal = normalTransform * pMesh->mNormals[index];
//		*dest++ = normal.x_;
//		*dest++ = normal.y_;
//		*dest++ = normal.z_;
//	}
//
//	if (pMesh->mUVs_.Size() > 0u)
//	{
//		Vector2 uv = pMesh->mUVs_[index];
//		*dest++ = uv.x_;
//		*dest++ = uv.y_;
//	}
//}


// ------ Helpers ------
inline float SegLenght(Vector3& pos_a, Vector3& pos_b)
{
	float len = sqrtf(pow(pos_b.x_ - pos_a.x_, 2.0f)
		+ pow(pos_b.y_ - pos_a.y_, 2.0f)
		+ pow(pos_b.z_ - pos_a.z_, 2.0f));

	return len;
}
