#pragma once
#include <string>

// Back image path 
std::string g_Back_Img_Path_;
std::string g_M_49_Path_;

std::string g_Cloth_Img_Path_;
std::string g_Cloth_Contour_Path_;
std::string g_Cloth_Mesh_Path_;

// out texture path
std::string g_Out_Path_;

int Transfer(const std::string b_i, // backimage
			  const std::string m_49, // 49 points
			  const std::string c_i, // cloth image
			  const std::string c_c, // cloth contour
			  const std::string c_m, // cloth mesh
			  const std::string out); // output path